/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "PCLCloudSurface.h"

#define DEFAULT_POLYNOMIAL_ORDER 2

PCLCloudSurface::PCLCloudSurface() { }

void PCLCloudSurface::AddLeapFrame(std::vector<float> points) 
{
	this->points_cloud->points.clear();
    for (int i = 0; i < points.size(); i += 3) {
        pcl::PointXYZ point;
        point.x = points[i];
        point.y = points[i + 1];
        point.z = points[i + 2];
        this->points_cloud->points.push_back(point);
    }
}

void PCLCloudSurface::GenerateNormals() 
{
    this->mls.setInputCloud(this->points_cloud);
    this->mls.setComputeNormals(true);
    this->mls.setPolynomialOrder(2);
    // this->mls.setSearchMethod(tree);
    this->mls.setSearchRadius(50.0f);
    // this->mls.setPolynomialFit(true);
	this->mls.setPolynomialOrder(DEFAULT_POLYNOMIAL_ORDER);
    this->normals_cloud->clear();
    this->mls.process(*this->normals_cloud);
}

void PCLCloudSurface::PerformUpsampling() 
{
    this->mls.setUpsamplingMethod(pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal>::SAMPLE_LOCAL_PLANE);
	this->mls.setUpsamplingRadius(0.005f);
	this->mls.setUpsamplingStepSize(0.005f);
}

glm::vec3 PCLCloudSurface::ProjectPointToMLSSurface(glm::vec3 point)
{
	return glm::vec3(0.0f, 0.0f, 0.0f);
}

std::vector<glm::vec3>& PCLCloudSurface::ProjectPointsToMLSSurface(std::vector<glm::vec3>& points)
{
	return points;
}

std::vector<float>& PCLCloudSurface::GetProjectedPoints() {
    std::vector<float> projected_points;
	for (int i = 0; i < this->normals_cloud->points.size(); ++i)
    {
        projected_points.push_back(this->normals_cloud->points[i].x);
        projected_points.push_back(this->normals_cloud->points[i].y);
        projected_points.push_back(this->normals_cloud->points[i].z);
    }

    return projected_points;
}

std::vector<float>& PCLCloudSurface::GetNormalPoints()
{
    std::vector<float> normal_frame_points;
	for (int i = 0; i < this->normals_cloud->points.size(); ++i)
    {
        normal_frame_points.push_back(this->normals_cloud->points[i].x);
        normal_frame_points.push_back(this->normals_cloud->points[i].y);
        normal_frame_points.push_back(this->normals_cloud->points[i].z);
    }

    return normal_frame_points;
}

/*std::vector<float>& GetKNNAverageDistance()
{
	return std::vector<float>();
}*/