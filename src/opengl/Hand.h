/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __HAND_H__
#define __HAND_H__
#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <LeapC.h>
#include "Object.h"
#include "LeapConnection.h"
#include "Program.h"
#include "VertexArray.h"
#include "VertexBuffer.h"

class Hand : public Object
{
private:
	VertexArray* vao;
	VertexBuffer* vbo[2];
	Program* program;
	std::vector<GLfloat> frame_points;
	std::vector<GLfloat> frame_normals;
	glm::vec3 position;
	float angle;
	glm::vec3 scale;

	glm::mat4 model_matrix;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;

public:
	Hand(glm::vec3 position = glm::vec3(0.0f), float angle = 0.0f, glm::vec3 scale = glm::vec3(1.0f));
	virtual ~Hand() = default;

	void Init() override;
	void Render(Camera camera) override;
	void Update(glm::mat4 view_matrix, glm::mat4 projection_matrix) override;
	void Clean() override;

};

#endif // __HAND_H__