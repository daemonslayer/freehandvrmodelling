/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __BOUNDINGSPHERE_H__
#define __BOUNDINGSPHERE_H__

#include <glm/glm.hpp>
#include "Raycast.h"

class BoundingSphere
{
private:
	glm::vec3 center;
	float radius;

public:
	BoundingSphere(glm::vec3 center = glm::vec3(0.0f, 0.0f, 0.0f), float radius = 1.0f);
	virtual ~BoundingSphere();

	void SetCenter(glm::vec3 center);
	glm::vec3 GetCenter();
	void SetRadius(float radius);
	float GetRadius();

};

#endif