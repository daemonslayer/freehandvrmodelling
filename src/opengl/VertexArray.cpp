/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "VertexArray.h"
#include <iostream>


VertexArray::VertexArray() { glGenVertexArrays(1, &this->vao); }
VertexArray::VertexArray(const VertexArray& varray) { }
VertexArray::operator GLuint() const { return this->vao; }

void VertexArray::BindAttribute(const Attribute& attribute, const VertexBuffer& buffer, int Type, uint count, uint stride, intptr_t offset)
{
	glBindVertexArray(this->vao);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glVertexAttribPointer(attribute, count, Type, GL_FALSE, stride, (const GLvoid*)offset);
	glEnableVertexAttribArray(attribute);
}

void VertexArray::BindElements(const VertexBuffer& elements)
{
	glBindVertexArray(this->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements);
}

void VertexArray::BindTransformFeedback(uint index, const VertexBuffer& buffer)
{
	glBindVertexArray(this->vao);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, index, buffer);
}

VertexArray::~VertexArray() { }