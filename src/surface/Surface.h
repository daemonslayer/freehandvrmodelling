#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "Object.h"
#include "BucketHolder.h"
#include "PCLCloudSurface.h"
#include "LeapConnection.h"
#include "VertexBuffer.h"
#include "VertexArray.h"
#include "Program.h"
#include "AverageFrame.h"
#include "FrameSampler.h" 

class Surface : public Object
{
private:
	VertexArray* vao;
	VertexBuffer* vbo[2];
	Program* program;
	BucketHolder bucketHolder;
	std::vector<float> frame_points;
	std::vector<float> frame_projected_points;
	std::vector<float> frame_normals;
	PCLCloudSurface surface;
	FrameSampler sampler;
	glm::vec3 position;
	float angle;
	glm::vec3 scale;

	glm::mat4 model_matrix;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;

public:
	Surface(glm::vec3 position = glm::vec3(0.0f), float angle = 0.0f, glm::vec3 scale = glm::vec3(0.0f));
	~Surface();

	void Init() override;
	void Render(Camera camera) override;
	void Update(glm::mat4 view_matrix, glm::mat4 projection_matrix) override;
	void Clean() override;

};

