#pragma once

#include <cstdint>
#include <SDL.h>

class FPSCounter
{
private:
	uint32_t start_time;
	uint32_t pause_time;
	bool paused;
	bool started;

public:
	FPSCounter();
	virtual ~FPSCounter();

	void Start();
	void Stop();
	void Pause();
	void Play();
	uint32_t GetTicks();
	bool IsStarted();
	bool IsPaused();
};

