/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "Hand.h"
#include <iostream>

Hand::Hand(glm::vec3 position, float angle, glm::vec3 scale) :
	position(position),
	angle(angle),
	scale(scale)
{ 
	Shader c_vert(ShaderType::Vertex, "res/shaders/hands_vs.glsl");
	Shader c_frag(ShaderType::Fragment, "res/shaders/hands_fs.glsl");
	program = new Program(c_vert, c_frag);

	vbo[0] = new VertexBuffer(&frame_points[0], frame_points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	// vbo[1] = new VertexBuffer(&cube_normals[0], cube_normals.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vao = new VertexArray();
	vao->BindAttribute(program->GetAttribute("position"), *vbo[0], GL_FLOAT, 3, 0, 0);
	// vao->BindAttribute(program->GetAttribute("normal"), *vbo[1], GL_FLOAT, 3, 0, 0);
	this->Init();
}

void Hand::Init()
{
	this->model_matrix = glm::scale(this->model_matrix, this->scale);
	this->model_matrix = glm::rotate(this->model_matrix, this->angle, glm::vec3(1.0f));
	this->model_matrix = glm::translate(this->model_matrix, this->position);
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
	glUseProgram(0);
}

void Hand::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());

	if (!IsConnected) exit(0);
	const LEAP_TRACKING_EVENT* frame = GetFrame();
	if (frame->nHands > 0) {
		frame_points.clear();
		for (int i = 0; i < frame->nHands; ++i) {
			const LEAP_HAND* hand = &frame->pHands[i];
			for (int f = 0; f < 5; ++f)
			{
				LEAP_DIGIT finger = hand->digits[f];
				for (int b = 0; b < 4; ++b)
				{
					LEAP_BONE bone = finger.bones[b];
					this->frame_points.push_back(bone.next_joint.x);
					this->frame_points.push_back(bone.next_joint.y);
					this->frame_points.push_back(bone.next_joint.z);
				}
			}
		}
	}

	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	vbo[0]->Data(&frame_points[0], frame_points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	glBindVertexArray(*this->vao);
	glDrawArrays(GL_LINES, 0, this->frame_points.size()/3);
	glUseProgram(0);
}

void Hand::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;

	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void Hand::Clean()
{
	this->frame_points.clear();
	this->frame_normals.clear();
}