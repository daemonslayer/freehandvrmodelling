#include <iostream>
#include <glm/glm.hpp>
#include <algorithm>
#include <SDL.h>
#undef main
#include "Window.h"
#include "Context.h"
#include "Camera.h"
#include "Cube.h"

glm::vec3 getTrackBallVector(double x, double y)
{
	glm::vec3 p = glm::vec3(2.0*x / (float)WINDOW_WIDTH - 1.0, 2.0*y / (float)WINDOW_HEIGHT - 1.0, 0.0);
	p.y = -p.y;

	float mag2 = p.x*p.x + p.y*p.y;
	if (mag2 <= 1.0f)
		p.z = sqrtf(1.0f - mag2);
	else
		p = glm::normalize(p);
	return p;
}

int main(int argc, char* argv)
{
	Window window("Test : CubeRotation", WINDOW_WIDTH, WINDOW_HEIGHT);
	Context& context = window.GetWindowContext();
	context.GlewSetup();
	context.EnableGL();

	Camera camera(glm::vec3(0.0f, -300.0f, -500.0f));
	Cube cube;
	cube.Init(camera);

	bool done = false, p_open = true;
	bool is_dragging = false;
	Uint32 prev_leftbtn_state = SDL_MOUSEBUTTONUP;
	Uint32 curr_leftbtn_state = SDL_MOUSEBUTTONUP;
	double current_x = 0, current_y = 0, old_x = 0, old_y = 0;
	int x = 0, y = 0;

	while (!done)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			ImGui_ImplSDL2_ProcessEvent(&event);
			curr_leftbtn_state = event.type;
			SDL_GetMouseState(&x, &y);

			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window.GetWindow()))
				done = true;
			if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
				done = true;
			if (event.button.button == SDL_BUTTON_LEFT && curr_leftbtn_state == SDL_MOUSEBUTTONDOWN && prev_leftbtn_state == SDL_MOUSEBUTTONUP) {
				is_dragging = true;
				current_x = old_x = x;
				current_y = old_y = y;
			}
			if (event.button.button == SDL_BUTTON_LEFT && curr_leftbtn_state == SDL_MOUSEBUTTONDOWN && prev_leftbtn_state == SDL_MOUSEBUTTONDOWN) {
				current_x = x;
				current_y = y;
			}
			if (event.button.button == SDL_BUTTON_LEFT && curr_leftbtn_state == SDL_MOUSEBUTTONUP && prev_leftbtn_state == SDL_MOUSEBUTTONDOWN)
				is_dragging = false;
		}
		prev_leftbtn_state = curr_leftbtn_state;

		context.Clear();
		context.ClearColor(glm::vec4(glm::vec3(0.9f), 1.0f));

		if (is_dragging && (current_x != old_x || current_y != old_y))
		{
			glm::vec3 va = getTrackBallVector(old_x, old_y);
			glm::vec3 vb = getTrackBallVector(current_x, current_y);

			float angle = acos(std::min(1.0f, glm::dot(va, vb)));
			glm::vec3 axis_in_camera_coord = glm::cross(va, vb);
			glm::mat3 camera2object = glm::inverse(glm::mat3(camera.GetViewMatrix() * cube.GetModelMatrix()));
			glm::vec3 axis_in_object_coord = camera2object * axis_in_camera_coord;
			cube.Update(angle, camera.GetPosition(), axis_in_object_coord);
			old_x = current_x;
			old_y = current_y;
		}
		cube.Render();

		SDL_GL_MakeCurrent(context.GetWindow(), *context.GetWindowContext());
		SDL_GL_SwapWindow(context.GetWindow());
	}
}