/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include <iostream>
#include <fstream>
#include <SDL.h>
#undef main
#include "imgui.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"
#include <GL/glew.h>

#include "Platform.h"
#include "Window.h"
#include "Context.h"
#include "Camera.h"
#include "Grid.h"
#include "Gizmo.h"
#include "Cube.h"
#include "Hand.h"
#include "FrameSampler.h"
#include "ViewTransformation.h"
#include "Surface.h"
#include "Pointer.h"
#include "Gesture.h"
#include "GestureListener.h"
#include "Model.h"
#include "FPSCounter.h"

int32_t lastDrawnFrameId = 0;
volatile int32_t newestFrameId = 0;
void OnFrame(const LEAP_TRACKING_EVENT* frame) 
	{ newestFrameId = (int32_t)frame->tracking_frame_id; }

int main(int argc, char** argv)
{
	ConnectionCallbacks.on_frame = OnFrame;
	OpenConnection();
	while (!IsConnected) {
		millisleep(250);
	}

	Window window("FreehandVRModelling", WINDOW_WIDTH, WINDOW_HEIGHT);
	Context& context = window.GetWindowContext();
	context.GlewSetup();
	context.EnableGL();
	FPSCounter fps_counter;
	fps_counter.Start();

	Camera camera(glm::vec3(0.0f, 0.0f, -10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	Grid grid(100, 10.0f, glm::vec3(0.0f, -10.0f, 0.0f));
	Gizmo gizmo(glm::vec3(0.0f, 0.0f, -5.0f));
	Cube cube(glm::vec3(0.0f, 0.0f, 0.0f), 0.0f);
	Hand hand(glm::vec3(0.0f, 100.0f, -10.0f));
	Pointer pointer;
	Model hand_model("res/models/hand/hand.obj");
	Surface surface(glm::vec3(0.0f, 100.0f, 250.0f));
	ViewTransformation transformation;
	int count_frames = 0;

	GestureListener gesture_listener;
	gesture_listener.EnableGesture(GESTURE_TYPE::FINGERPOINT_GESTURE);
	gesture_listener.EnableGesture(GESTURE_TYPE::FACINGHANDS_GESTURE);

	context.ClearColor();
	bool done = false, p_open = true;
	while (!done)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			ImGui_ImplSDL2_ProcessEvent(&event);
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window.GetWindow()))
				done = true;
			if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
				done = true;
		}
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame(context.GetWindow());
		ImGui::NewFrame();
		
		++count_frames;
		float avg_fps = count_frames / (fps_counter.GetTicks() / 1000.f);
		if (avg_fps > 2000000)
			avg_fps = 0;
		
		if (!IsConnected) exit(0);
		const LEAP_TRACKING_EVENT* frame = GetFrame();

		{
			const float DISTANCE = 10.0f;
			const char* message_str = "FreehandVRModelling : Leap Hand";
			std::string sdl_framerate_str = "SDL FrameRate : " + std::to_string((int)avg_fps);
			std::string leap_framerate_str = "Leap FrameRate : " + std::to_string((int)frame->framerate);
			int corner = 0;
			ImVec2 window_pos = ImVec2((corner & 1) ? ImGui::GetIO().DisplaySize.x - DISTANCE : DISTANCE,
				(corner & 2) ? ImGui::GetIO().DisplaySize.y - DISTANCE : DISTANCE);
			ImVec2 window_pos_pivot = ImVec2((corner & 1) ? 1.0f : 0.0f, (corner & 2) ? 1.0f : 0.0f);
			if (corner != -1)
				ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
			ImGui::SetNextWindowBgAlpha(0.05f); // Transparent background
			if (ImGui::Begin("OverlayMessage", &p_open, (corner != -1 ? ImGuiWindowFlags_NoMove : 0) |
				ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize |
				ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav))
			{
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));
				ImGui::Text(message_str);
				ImGui::Text(sdl_framerate_str.c_str());
				ImGui::Text(leap_framerate_str.c_str());
				ImGui::PopStyleColor();
			}
			ImGui::End();
		}

		ImGui::Render();
		SDL_GL_MakeCurrent(context.GetWindow(), *context.GetWindowContext());
		context.Clear();
		context.ClearColor(glm::vec4(glm::vec3(0.97f), 1.0f));
		
		camera.UpdateLookAt();
		grid.Render(camera);
		gizmo.Render(camera);
		surface.Render(camera);
		cube.Render(camera);
		hand.Render(camera);

		switch (gesture_listener.CheckFrameForGesture()) {
		case GESTURE_TYPE::HOLD_GESTURE:
		{
			std::cout << "Hand Hold Gesture recorded" << std::endl;
			break;
		}

		case GESTURE_TYPE::FINGERPOINT_GESTURE:
		{
			pointer.Render(camera);
			break;
		}

		case GESTURE_TYPE::FACINGHANDS_GESTURE:
		{
			transformation.AddFrame();
			glm::vec3 axis = transformation.GetAxis();
			glm::vec3 pivot = transformation.GetPivot();
			float angle = transformation.GetAngle();
			float scale = transformation.GetDistanceBetweenHands();
			glm::vec3 zoom = transformation.GetZoom();

			{
				/*std::cout << "Axis : " << axis.x << " " << axis.y << " " << axis.z << std::endl;
				std::cout << "Pivot : " << pivot.x << " " << pivot.y << " " << pivot.z << std::endl;
				std::cout << "Angle : " << angle << std::endl;
				std::cout << "Scale : " << scale << std::endl;
				std::cout << "Zoom : " << zoom.x << " " << zoom.y << " " << zoom.z << std::endl << std::endl;*/
			}
			axis = glm::vec3(0.0f, 1.0f, 0.0f);
			pivot = glm::vec3(0.0f);
			cube.Update(angle * 100.0f, pivot, axis);
			// scale down by 50: translation too much otherwise
			// camera.OffsetPosition(glm::vec3(0.0f, 0.0f, std::max(std::max(zoom.x, zoom.y), zoom.z) / 50));
			// glm::mat4 projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
			//	(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());
			// cube.Update(camera.GetViewMatrix(), projection_matrix);
		}

		default:
			// std::cout << "No Gesture Recorded" << std::endl;
			break;
		}

		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		SDL_GL_SwapWindow(context.GetWindow());
	}

	return 0;
}