/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __WINDOW_H__
#define __WINDOW_H__
#pragma once

#include <SDL.h>
#include "Platform.h"
#include "Context.h"
#include "imgui.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"

class Context;
class Window
{
private:
	SDL_GLContext gl_context;
	SDL_Window* gl_window;
	Context* context;
	int gl_window_width;
	int gl_window_height;

	void InitSDL();
	void InitImgui();

public:
	Window(const char* window_title = "FreehandVRModelling");
	Window(const char* window_title, int width, int height);
	~Window();

	Context& GetWindowContext();
	SDL_Window* GetWindow();
	void ToggleFullScreen();
};

#endif // __WINDOW_H__