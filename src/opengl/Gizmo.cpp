/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "Gizmo.h"

Gizmo::Gizmo(glm::vec3 position, float angle, glm::vec3 scale) :
	position(position),
	angle(angle),
	scale(scale)
{ 
	this->frame_points.push_back(1.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);

	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(1.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);

	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(1.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);
	this->frame_points.push_back(0.0f);

	Shader c_vert(ShaderType::Vertex, "res/shaders/hands_vs.glsl");
	Shader c_frag(ShaderType::Fragment, "res/shaders/hands_fs.glsl");
	program = new Program(c_vert, c_frag);

	vbo = new VertexBuffer(&frame_points[0], frame_points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vao = new VertexArray();
	vao->BindAttribute(program->GetAttribute("position"), *vbo, GL_FLOAT, 3, 0, 0);
	this->Init();
}

void Gizmo::Init()
{
	this->model_matrix = glm::scale(this->model_matrix, this->scale);
	this->model_matrix = glm::rotate(this->model_matrix, glm::radians(this->angle), glm::vec3(1.0f));
	this->model_matrix = glm::translate(this->model_matrix, glm::vec3(this->position));
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
	glUseProgram(0);
}

void Gizmo::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());
	glUseProgram(*(this->program));
	this->program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	this->program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	vbo->Data(&frame_points[0], frame_points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	glBindVertexArray(*this->vao);
	glDrawArrays(GL_LINES, 0, this->frame_points.size()/3);
	glUseProgram(0);
}

void Gizmo::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;
	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void Gizmo::Clean()
{
	this->frame_points.clear();
	this->model_matrix = glm::mat4(1.0f);
	this->view_matrix = glm::mat4(1.0f);
	this->projection_matrix = glm::mat4(1.0f); 
}


Gizmo::~Gizmo() { this->Clean(); }