/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __BUCKETHOLDER_H__
#define __BUCKETHOLDER_H__
#pragma once

#include <vector>
#include <functional>
#include "Bucket.h"

class BucketHolder {
private:
    static int size_of_vector;
    static bool sizeInitialized;
    uint64_t total_points;
    std::vector<std::reference_wrapper<Bucket> > buckets;
    std::vector<GLuint> vbo_buckets;
    Bucket* topBucket;
    static GLuint vao_bucket_holder;

    void SetTopBucket(int bucket_id);
    void AddBucket();
    void SetupVertexBuffers();
    static void SetupVertexArray();
    void PushVertexBuffersToArray();

public:
    BucketHolder();
    BucketHolder(int size_of_vector);
    virtual ~BucketHolder() = default;

    static void SetSizeOfPoints(int size);
    int GetSizeOfPoints();
    Bucket* GetTopBucket();
    int GetTopBucketID();
    uint64_t PointsCount();
    GLuint GetVertexArray();
    void AddPoints(std::vector<float> points);
    void DisplayPoints();
};

#endif  // __BUCKETHOLDER_H__