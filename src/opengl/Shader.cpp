/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "Shader.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <streambuf>

Shader::Shader(ShaderType type) { }

Shader::Shader(ShaderType type, const std::string & url)
 : shader(-1) {
	this->shader = glCreateShader(type);
	Source(url);
	Compile();
}

Shader::operator GLuint() const { return this->shader; }

const Shader& Shader::operator=(const Shader & other)
{
	// TODO: insert return statement here
	return *this;
}

void Shader::Source(const std::string& url)
{
	std::ifstream code(url);
	if (!code) {
		std::cout << "[!] Shader file could not be opened" << std::endl;
		exit(0);
	}
	std::string str;
	code.seekg(0, std::ios::end);
	str.reserve(code.tellg());
	code.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(code)),
		std::istreambuf_iterator<char>());
	const GLchar* shader_source = (const GLchar*)str.data();
	glShaderSource(this->shader, 1, &shader_source, NULL);
}

void Shader::Compile()
{
	GLint res = -1;

	glCompileShader(this->shader);
	glGetShaderiv(this->shader, GL_COMPILE_STATUS, &res);
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	glGetShaderInfoLog(this->shader, max_length, &actual_length, log);
	std::cout << "Logger value of " << this->shader << " : " << log << std::endl;

	if (res == GL_FALSE)
		throw CompileException(GetInfoLog());
}

std::string Shader::GetInfoLog()
{
	GLint res = -1;
	glGetShaderiv(this->shader, GL_INFO_LOG_LENGTH, &res);

	if (res > 0)
	{
		std::string infoLog(res, 0);
		glGetShaderInfoLog(this->shader, res, &res, &infoLog[0]);
		return infoLog;
	}
	else {
		return "";
	}
}
