/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "PinchDrawDetector.h"
#include <iostream>


PinchDrawDetector::PinchDrawDetector() :
	gesture_type(GESTURE_TYPE::PINCHSWIPE_GESTURE)
{
	this->InitCompositeGesture();
	this->AddGesture(GESTURE_TYPE::PINCH_GESTURE, 
		LOGICGATE_TYPE::AND_GATE, false, std::chrono::seconds(0));
	this->AddGesture(GESTURE_TYPE::SWIPE_GESTURE,
		LOGICGATE_TYPE::AND_GATE, false, std::chrono::seconds(10));
}

GESTURE_TYPE PinchDrawDetector::Type() { return this->gesture_type; }

void PinchDrawDetector::RegisterGestureListener(IGestureListener* listener) 
{

}

void PinchDrawDetector::RemoveGestureListener(IGestureListener* listener) 
{

}

void PinchDrawDetector::NotifyGestureListener(bool is_active) 
{

}