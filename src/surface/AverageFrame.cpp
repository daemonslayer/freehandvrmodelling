#include "AverageFrame.h"
#include <iostream>

AverageFrame::AverageFrame()
{
	this->avg_frame = new LEAP_TRACKING_EVENT();
}

void AverageFrame::AddCurrentFrame()
{
	if (!IsConnected) exit(0);
	const LEAP_TRACKING_EVENT* frame = GetFrame();
	if (this->frames.size() >= POOL_SIZE) this->RemoveFirstFrame();
	this->frames.push_back(frame);
}

void AverageFrame::AddFrame(const LEAP_TRACKING_EVENT* frame)
{
	if (this->frames.size() >= POOL_SIZE) this->RemoveFirstFrame();
	this->frames.push_back(frame);
}

const LEAP_TRACKING_EVENT* AverageFrame::GetFirstFrame() { return this->frames[0]; }
const LEAP_TRACKING_EVENT* AverageFrame::operator[] (int index) { return this->frames[index]; }
AverageFrame::operator const LEAP_TRACKING_EVENT*() { return this->avg_frame; }
void AverageFrame::RemoveFrame(int index) { this->frames.erase(this->frames.begin() + index); }
void AverageFrame::RemoveFirstFrame() { this->frames.erase(this->frames.begin()); }
void AverageFrame::RemoveLastFrame() { this->frames.erase(this->frames.end()); }
bool AverageFrame::IsFull() { return this->frames.size() >= POOL_SIZE; }
bool AverageFrame::IsEmpty() { return this->frames.empty(); }
int AverageFrame::Size() { return this->frames.size(); }

void log_frame(const LEAP_TRACKING_EVENT* frame, const char* msg)
{
	std::ofstream log_file;
	log_file.open("log.txt", std::ios_base::app);
	log_file << msg << std::endl;
	log_file << frame->pHands[0].palm.position.x << " " << frame->pHands[0].palm.position.y << " " << frame->pHands[0].palm.position.z << std::endl;
	log_file << frame->pHands[1].palm.position.x << " " << frame->pHands[1].palm.position.y << " " << frame->pHands[1].palm.position.z << std::endl;
	log_file << std::endl;
}

const LEAP_TRACKING_EVENT* AverageFrame::GetAverageFrame()
{
	LEAP_TRACKING_EVENT* frame = new LEAP_TRACKING_EVENT();
	LEAP_FRAME_HEADER frame_header = { 0 };
	LEAP_HAND left_hand;
	LEAP_HAND right_hand;
	LEAP_HAND hands[2];

	frame->info = frame_header;
	frame->tracking_frame_id = 0;
	frame->nHands = 2;

	left_hand.palm.position.x
		= left_hand.palm.position.y
		= left_hand.palm.position.z = 0.0f;
	left_hand.palm.normal.x
		= left_hand.palm.normal.y
		= left_hand.palm.normal.z = 0.0f;
	for (int f = 0; f < 5; ++f) {
		left_hand.digits[f].bones[0].prev_joint.x
			= left_hand.digits[f].bones[0].prev_joint.y
			= left_hand.digits[f].bones[0].prev_joint.z = 0.0f;
		for (int b = 0; b < 4; ++b) {
			left_hand.digits[f].bones[b].next_joint.x
				= left_hand.digits[f].bones[b].next_joint.y
				= left_hand.digits[f].bones[b].next_joint.z = 0.0f;
		}
	}
	right_hand.palm.position.x
		= right_hand.palm.position.y
		= right_hand.palm.position.z = 0.0f;
	right_hand.palm.normal.x
		= right_hand.palm.normal.y
		= right_hand.palm.normal.z = 0.0f;
	for (int f = 0; f < 5; ++f) {
		right_hand.digits[f].bones[0].prev_joint.x
			= right_hand.digits[f].bones[0].prev_joint.y
			= right_hand.digits[f].bones[0].prev_joint.z = 0.0f;
		for (int b = 0; b < 4; ++b) {
			right_hand.digits[f].bones[b].next_joint.x
				= right_hand.digits[f].bones[b].next_joint.y
				= right_hand.digits[f].bones[b].next_joint.z = 0.0f;
		}
	}
	
	int framerate = 0;

	for (int i = 0; i < this->frames.size(); ++i) {
		framerate += this->frames[i]->framerate;
		if (this->frames[i]->nHands > 0) {
			for (int j = 0; j < this->frames[i]->nHands; ++j) {
				if (frames[i]->pHands[j].type == eLeapHandType_Left) {
					left_hand.palm.position.x += frames[i]->pHands[j].palm.position.x;
					left_hand.palm.position.y += frames[i]->pHands[j].palm.position.y;
					left_hand.palm.position.z += frames[i]->pHands[j].palm.position.z;
					left_hand.palm.normal.x += frames[i]->pHands[j].palm.normal.x;
					left_hand.palm.normal.y += frames[i]->pHands[j].palm.normal.y;
					left_hand.palm.normal.z += frames[i]->pHands[j].palm.normal.z;

					for (int f = 0; f < 5; ++f) {
						left_hand.digits[f].bones[0].prev_joint.x += frames[i]->pHands[j].digits[f].bones[0].prev_joint.x;
						left_hand.digits[f].bones[0].prev_joint.y += frames[i]->pHands[j].digits[f].bones[0].prev_joint.y;
						left_hand.digits[f].bones[0].prev_joint.z += frames[i]->pHands[j].digits[f].bones[0].prev_joint.z;
						for (int b = 0; b < 4; ++b) {
							left_hand.digits[f].bones[b].next_joint.x += frames[i]->pHands[j].digits[f].bones[b].next_joint.x;
							left_hand.digits[f].bones[b].next_joint.y += frames[i]->pHands[j].digits[f].bones[b].next_joint.y;
							left_hand.digits[f].bones[b].next_joint.z += frames[i]->pHands[j].digits[f].bones[b].next_joint.z;
						}
					}
				}

				else {
					right_hand.palm.position.x += frames[i]->pHands[j].palm.position.x;
					right_hand.palm.position.y += frames[i]->pHands[j].palm.position.y;
					right_hand.palm.position.z += frames[i]->pHands[j].palm.position.z;
					right_hand.palm.normal.x += frames[i]->pHands[j].palm.normal.x;
					right_hand.palm.normal.y += frames[i]->pHands[j].palm.normal.y;
					right_hand.palm.normal.z += frames[i]->pHands[j].palm.normal.z;

					for (int f = 0; f < 5; ++f) {
						right_hand.digits[f].bones[0].prev_joint.x += frames[i]->pHands[j].digits[f].bones[0].prev_joint.x;
						right_hand.digits[f].bones[0].prev_joint.y += frames[i]->pHands[j].digits[f].bones[0].prev_joint.y;
						right_hand.digits[f].bones[0].prev_joint.z += frames[i]->pHands[j].digits[f].bones[0].prev_joint.z;
						for (int b = 0; b < 4; ++b) {
							right_hand.digits[f].bones[b].next_joint.x += frames[i]->pHands[j].digits[f].bones[b].next_joint.x;
							right_hand.digits[f].bones[b].next_joint.y += frames[i]->pHands[j].digits[f].bones[b].next_joint.y;
							right_hand.digits[f].bones[b].next_joint.z += frames[i]->pHands[j].digits[f].bones[b].next_joint.z;
						}
					}
				}
			}
		}
	}

 	if(frames.size() > 0) {
		left_hand.palm.position.x /= frames.size();
		left_hand.palm.position.y /= frames.size();
		left_hand.palm.position.z /= frames.size();
		left_hand.palm.normal.x /= frames.size();
		left_hand.palm.normal.y /= frames.size();
		left_hand.palm.normal.z /= frames.size();
		for (int f = 0; f < 5; ++f) {
			left_hand.digits[f].bones[0].prev_joint.x /= frames.size();
			left_hand.digits[f].bones[0].prev_joint.y /= frames.size();
			left_hand.digits[f].bones[0].prev_joint.z /= frames.size();
			for (int b = 0; b < 4; ++b) {
				left_hand.digits[f].bones[b].next_joint.x /= frames.size();
				left_hand.digits[f].bones[b].next_joint.y /= frames.size();
				left_hand.digits[f].bones[b].next_joint.z /= frames.size();
			}
		}
		right_hand.palm.position.x /= frames.size();
		right_hand.palm.position.y /= frames.size();
		right_hand.palm.position.z /= frames.size();
		right_hand.palm.normal.x /= frames.size();
		right_hand.palm.normal.y /= frames.size();
		right_hand.palm.normal.z /= frames.size();
		for (int f = 0; f < 5; ++f) {
			right_hand.digits[f].bones[0].prev_joint.x /= frames.size();
			right_hand.digits[f].bones[0].prev_joint.y /= frames.size();
			right_hand.digits[f].bones[0].prev_joint.z /= frames.size();
			for (int b = 0; b < 4; ++b) {
				right_hand.digits[f].bones[b].next_joint.x /= frames.size();
				right_hand.digits[f].bones[b].next_joint.y /= frames.size();
				right_hand.digits[f].bones[b].next_joint.z /= frames.size();
			}
		}
	}

	left_hand.id = 0;
	left_hand.type = eLeapHandType_Left;
	right_hand.id = 1;
	right_hand.type = eLeapHandType_Right;
	hands[0] = left_hand;
	hands[1] = right_hand;
	frame->pHands = hands;
	this->avg_frame->pHands = hands;
	if (frames.size() > 0) {
		frame->framerate = framerate / frames.size();
		this->avg_frame->framerate = framerate / frames.size();
	}

	// this->avg_frame = frame;
	log_frame(this->avg_frame, "Frame");
	return this->avg_frame;
}

void AverageFrame::Clear() { this->frames.clear(); }