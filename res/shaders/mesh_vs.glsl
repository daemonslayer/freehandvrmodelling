#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex_coords;

out vec3 fposition;
out vec3 fnormal;
out vec2 ftex_coords;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 normalsMatrix;

void main()
{
	fposition = position;
	fnormal = normal;
	ftex_coords = tex_coords;
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0f);
}