#include "MeshAnimation.h"
#include <assert.h>

MeshAnimation::MeshAnimation(Model& model) :
	scene(model.scene) { }
MeshAnimation::MeshAnimation(Model& model, std::vector<MeshVertexBoneData> bone_id_weights) :
	scene(model.scene),
	bones_id_weights_per_vertex(bone_id_weights)
{
	this->SetupAnimation();
}

void MeshVertexBoneData::AddBoneData(unsigned int bone_id, float weight)
{
	for (unsigned int i = 0; i < NUM_BONES_PER_VERTEX; i++) {
		if (Weights[i] == 0.0) {
			IDs[i] = bone_id;
			Weights[i] = weight;
			return;
		}
	}
}

unsigned int MeshAnimation::FindPosition(float p_animation_time, const aiNodeAnim* p_node_anim)
{
	for (unsigned int i = 0; i < p_node_anim->mNumPositionKeys - 1; i++) {
		if (p_animation_time < (float)p_node_anim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);
	return 0;
}

unsigned int MeshAnimation::FindRotation(float p_animation_time, const aiNodeAnim* p_node_anim)
{
	for (unsigned int i = 0; i < p_node_anim->mNumRotationKeys - 1; i++) {
		if (p_animation_time < (float)p_node_anim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);
	return 0;
}

unsigned int MeshAnimation::FindScaling(float p_animation_time, const aiNodeAnim* p_node_anim)
{
	for (unsigned int i = 0; i < p_node_anim->mNumScalingKeys - 1; i++) {
		if (p_animation_time < (float)p_node_anim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);
	return 0;
}

const aiNodeAnim* MeshAnimation::FindNodeAnim(const aiAnimation* p_animation, const std::string p_node_name)
{
	for (unsigned int i = 0; i < p_animation->mNumChannels; i++) {
		const aiNodeAnim* node_anim = p_animation->mChannels[i];
		if (std::string(node_anim->mNodeName.data) == p_node_name) {
			return node_anim;
		}
	}

	return nullptr;
}

// calculate transform matrix
aiVector3D MeshAnimation::CalcInterpolatedPosition(float p_animation_time, const aiNodeAnim* p_node_anim)
{
	if (p_node_anim->mNumPositionKeys == 1) {
		return p_node_anim->mPositionKeys[0].mValue;
	}

	unsigned int position_index = FindPosition(p_animation_time, p_node_anim);
	unsigned int next_position_index = position_index + 1;
	assert(next_position_index < p_node_anim->mNumPositionKeys);
	float delta_time = (float)(p_node_anim->mPositionKeys[next_position_index].mTime - p_node_anim->mPositionKeys[position_index].mTime);
	float factor = (p_animation_time - (float)p_node_anim->mPositionKeys[position_index].mTime) / delta_time;
	assert(factor >= 0.0f && factor <= 1.0f);
	aiVector3D start = p_node_anim->mPositionKeys[position_index].mValue;
	aiVector3D end = p_node_anim->mPositionKeys[next_position_index].mValue;
	aiVector3D delta = end - start;

	return start + factor * delta;
}

aiQuaternion MeshAnimation::CalcInterpolatedRotation(float p_animation_time, const aiNodeAnim* p_node_anim)
{
	if (p_node_anim->mNumRotationKeys == 1) {
		return p_node_anim->mRotationKeys[0].mValue;
	}

	unsigned int rotation_index = FindRotation(p_animation_time, p_node_anim);
	unsigned int next_rotation_index = rotation_index + 1;
	assert(next_rotation_index < p_node_anim->mNumRotationKeys);
	float delta_time = (float)(p_node_anim->mRotationKeys[next_rotation_index].mTime - p_node_anim->mRotationKeys[rotation_index].mTime);
	float factor = (p_animation_time - (float)p_node_anim->mRotationKeys[rotation_index].mTime) / delta_time;

	assert(factor >= 0.0f && factor <= 1.0f);
	aiQuaternion start_quat = p_node_anim->mRotationKeys[rotation_index].mValue;
	aiQuaternion end_quat = p_node_anim->mRotationKeys[next_rotation_index].mValue;

	return this->NLerp(start_quat, end_quat, factor);
}

aiVector3D MeshAnimation::CalcInterpolatedScaling(float p_animation_time, const aiNodeAnim* p_node_anim)
{
	if (p_node_anim->mNumScalingKeys == 1) {
		return p_node_anim->mScalingKeys[0].mValue;
	}

	unsigned int scaling_index = FindScaling(p_animation_time, p_node_anim);
	unsigned int next_scaling_index = scaling_index + 1;
	assert(next_scaling_index < p_node_anim->mNumScalingKeys);
	float delta_time = (float)(p_node_anim->mScalingKeys[next_scaling_index].mTime - p_node_anim->mScalingKeys[scaling_index].mTime);
	float  factor = (p_animation_time - (float)p_node_anim->mScalingKeys[scaling_index].mTime) / delta_time;
	assert(factor >= 0.0f && factor <= 1.0f);
	aiVector3D start = p_node_anim->mScalingKeys[scaling_index].mValue;
	aiVector3D end = p_node_anim->mScalingKeys[next_scaling_index].mValue;
	aiVector3D delta = end - start;

	return start + factor * delta;
}

void MeshAnimation::ReadNodeHierarchy(float p_animation_time, const aiNode* p_node, const aiMatrix4x4 parent_transform)
{
	std::string node_name(p_node->mName.data);

	const aiAnimation* animation = scene->mAnimations[0];
	aiMatrix4x4 node_transform = p_node->mTransformation;

	const aiNodeAnim* node_anim = FindNodeAnim(animation, node_name);

	if (node_anim) {

		//scaling
		//aiVector3D scaling_vector = node_anim->mScalingKeys[2].mValue;
		aiVector3D scaling_vector = CalcInterpolatedScaling(p_animation_time, node_anim);
		aiMatrix4x4 scaling_matr;
		aiMatrix4x4::Scaling(scaling_vector, scaling_matr);

		//rotation
		//aiQuaternion rotate_quat = node_anim->mRotationKeys[2].mValue;
		aiQuaternion rotate_quat = CalcInterpolatedRotation(p_animation_time, node_anim);
		aiMatrix4x4 rotate_matr = aiMatrix4x4(rotate_quat.GetMatrix());

		//translation
		//aiVector3D translate_vector = node_anim->mPositionKeys[2].mValue;
		aiVector3D translate_vector = CalcInterpolatedPosition(p_animation_time, node_anim);
		aiMatrix4x4 translate_matr;
		aiMatrix4x4::Translation(translate_vector, translate_matr);

		node_transform = translate_matr * rotate_matr * scaling_matr;
	}

	aiMatrix4x4 global_transform = parent_transform * node_transform;

	if (m_bone_mapping.find(node_name) != m_bone_mapping.end())	{
		unsigned int bone_index = m_bone_mapping[node_name];
		m_bone_matrices[bone_index].FinalWorldTransform = m_global_inverse_transform * global_transform * m_bone_matrices[bone_index].OffsetMatrix;
	}

	for (unsigned int i = 0; i < p_node->mNumChildren; i++) {
		ReadNodeHierarchy(p_animation_time, p_node->mChildren[i], global_transform);
	}
}

void MeshAnimation::BoneTransform(double time_in_sec, std::vector<aiMatrix4x4>& transforms)
{
	aiMatrix4x4 identity_matrix;
	// double time_in_ticks = time_in_sec * ticks_per_second;

	transforms.resize(m_num_bones);
	for (unsigned int i = 0; i < m_num_bones; i++) {
		transforms[i] = m_bone_matrices[i].FinalWorldTransform;
	}
}

void MeshAnimation::SetupAnimation()
{

}

glm::mat4 MeshAnimation::AiToGlm(aiMatrix4x4 ai_matr)
{
	return glm::mat4(1.0f);
}

aiQuaternion MeshAnimation::NLerp(aiQuaternion a, aiQuaternion b, float blend)
{
	return aiQuaternion();
}

MeshAnimation::~MeshAnimation() { }