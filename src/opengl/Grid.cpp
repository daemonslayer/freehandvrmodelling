/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "Grid.h"



Grid::Grid(int slices, float spacing, glm::vec3 position, float angle) :
	position(position),
	angle(angle)
{ 
	Shader g_vert(ShaderType::Vertex, "res/shaders/grid_vs.glsl");
	Shader g_frag(ShaderType::Fragment, "res/shaders/grid_fs.glsl");
	program = new Program(g_vert, g_frag);
	model_matrix = glm::mat4(1.0f);

	int half_slices = slices / 2;
	for (int i = -half_slices; i <= half_slices; ++i) {
		points.push_back((float)i*spacing);
		points.push_back(0.0f);
		points.push_back((float) -half_slices * spacing);
		points.push_back((float)i*spacing);
		points.push_back(0.0f);
		points.push_back((float)half_slices*spacing);

		points.push_back((float)-half_slices * spacing);
		points.push_back(0.0f);
		points.push_back((float)i*spacing);
		points.push_back((float)half_slices*spacing);
		points.push_back(0.0f);
		points.push_back((float)i*spacing);
	}

	vbo = new VertexBuffer(&points[0], points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vao = new VertexArray();
	vao->BindAttribute(program->GetAttribute("position"), *vbo, GL_FLOAT, 3, 0, 0);
	this->Init();
}

void Grid::Init()
{
	this->model_matrix = glm::rotate(this->model_matrix, this->angle, glm::vec3(1.0f, 0.0f, 0.0f));
	this->model_matrix = glm::translate(this->model_matrix, this->position);
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
	glUseProgram(0);
}

void Grid::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());

	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	vbo->Data(&points[0], points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	glBindVertexArray(*this->vao);
	glDrawArrays(GL_LINES, 0, this->points.size() / 3);
	glUseProgram(0);
}

void Grid::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;

	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void Grid::Clean()
{
	this->points.clear();
	this->normals.clear();
}

Grid::~Grid() { this->Clean(); }