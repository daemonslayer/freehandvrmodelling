#version 450
  
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in float radius;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out VertexData 
{
  vec3  normal;
  float radius;
} vertexOut;

void main()
{
  vertexOut.normal = normal;
  vertexOut.radius = radius;
  gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
}