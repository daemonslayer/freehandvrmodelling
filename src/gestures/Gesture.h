/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __GESTURE_H__
#define __GESTURE_H__
#pragma once

enum class HAND_TYPE
{
	NONE,
	LEFT_HAND,
	RIGHT_HAND,
	BOTH_HANDS,
};

enum class GESTURE_TYPE
{
	NONE,
	HOLD_GESTURE,
	EXTENDEDFINGER_GESTURE,
	PINCH_GESTURE,
	SWIPE_GESTURE,
	FINGERPOINT_GESTURE,
	FACINGHANDS_GESTURE,
	__GESTURELOGICGATE__,
	PINCHSWIPE_GESTURE,
	TWOHANDSWIPE_GESTURE,
	DOUBLETWOHANDHOLD_GESTURE,
};

enum class LOGICGATE_TYPE
{
	AND_GATE,
	OR_GATE,
};

#endif  // __GESTURE_H__
