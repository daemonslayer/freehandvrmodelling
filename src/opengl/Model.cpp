#include "Model.h"
#include <iostream>


Model::Model(bool mesh_animation) :
	scene(nullptr),
	path(""),
	mesh_animation(mesh_animation) { }
Model::Model(std::string path, bool mesh_animation) :
	scene(nullptr),
	path(path),
	mesh_animation(mesh_animation) 
{ 
	this->LoadModel(path);
}

void Model::LoadModel(std::string path)
{
	scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "error assimp : " << import.GetErrorString() << std::endl;
		return;
	}
	m_global_inverse_transform = scene->mRootNode->mTransformation;
	m_global_inverse_transform.Inverse();

	std::cout << "scene->HasAnimations() 1: " << scene->HasAnimations() << std::endl;
	std::cout << "scene->mNumMeshes 1: " << scene->mNumMeshes << std::endl;

	std::cout << "name nodes : " << std::endl;
	ShowNodeName(scene->mRootNode);
	std::cout << std::endl;

	std::cout << "name bones : " << std::endl;
	ProcessNode(scene->mRootNode, scene);

	this->Init();
}

void Model::ShowNodeName(aiNode* node)
{
	std::cout << node->mName.data << std::endl;
	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		ShowNodeName(node->mChildren[i]);
	}
}

void Model::ProcessNode(aiNode* node, const aiScene* scene)
{
	Mesh mesh;
	for (unsigned int i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* ai_mesh = scene->mMeshes[i];
		mesh = ProcessMesh(ai_mesh, scene);
		meshes.push_back(mesh); //accumulate all meshes in one vector
	}
}

Mesh Model::ProcessMesh(aiMesh* mesh, const aiScene* scene)
{
	std::vector<MeshVertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<MeshTexture> textures;

	vertices.reserve(mesh->mNumVertices); 
	indices.reserve(mesh->mNumVertices); 

	//vertices
	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		MeshVertex vertex;
		glm::vec3 vector;
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.Position = vector;

		if (mesh->mNormals != NULL)
		{
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;
		}
		else
		{
			vertex.Normal = glm::vec3();
		}


		// in assimp model can have 8 different texture coordinates
		// we only care about the first set of texture coordinates
		if (mesh->mTextureCoords[0])
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
		{
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}

	//indices
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i]; 
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}

	//material
	/*
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<MeshTexture> diffuse_maps = LoadMaterialTexture(material, aiTextureType_NORMALS, "texture_normal");
		bool exist = false;
		for (int i = 0; (i < textures.size()) && (diffuse_maps.size() != 0); i++)
		{
			if (textures[i].Path == diffuse_maps[0].Path)
			{
				exist = true;
			}
		}
		if (!exist && diffuse_maps.size() != 0) textures.push_back(diffuse_maps[0]);

		std::vector<MeshTexture> specular_maps = LoadMaterialTexture(material, aiTextureType_SPECULAR, "texture_specular");
		exist = false;
		for (int i = 0; (i < textures.size()) && (specular_maps.size() != 0); i++)
		{
			if (textures[i].Path == specular_maps[0].Path)
			{
				exist = true;
			}
		}
		if (!exist  && specular_maps.size() != 0) textures.push_back(specular_maps[0]); 

		std::vector<MeshTexture> ambient_maps = LoadMaterialTexture(material, aiTextureType_AMBIENT, "texture_ambient");
		exist = false;
		for (int i = 0; (i < textures.size()) && (ambient_maps.size() != 0); i++)
		{
			if (textures[i].Path == ambient_maps[0].Path)
			{
				exist = true;
			}
		}
		if (!exist  && specular_maps.size() != 0) textures.push_back(ambient_maps[0]);

	}
	*/

	// load bones
	/*
	for (unsigned int i = 0; i < mesh->mNumBones; i++)
	{
		unsigned int bone_index = 0;
		std::string bone_name(mesh->mBones[i]->mName.data);

		std::cout << mesh->mBones[i]->mName.data << std::endl;

		if (m_bone_mapping.find(bone_name) == m_bone_mapping.end()) 
		{
			// Allocate an index for a new bone
			bone_index = m_num_bones;
			m_num_bones++;
			BoneMatrix bi;
			m_bone_matrices.push_back(bi);
			m_bone_matrices[bone_index].offset_matrix = mesh->mBones[i]->mOffsetMatrix;
			m_bone_mapping[bone_name] = bone_index;

		}
		else
		{
			bone_index = m_bone_mapping[bone_name];
		}

		for (uint j = 0; j < mesh->mBones[i]->mNumWeights; j++)
		{
			uint vertex_id = mesh->mBones[i]->mWeights[j].mVertexId;
			float weight = mesh->mBones[i]->mWeights[j].mWeight;
			bones_id_weights_for_each_vertex[vertex_id].addBoneData(bone_index, weight);
		}
	}
	*/

	return Mesh(vertices, indices, textures);
}

GLuint Model::LoadImageToTexture(std::string image_path)
{
	ILuint ImageName; 
	ilGenImages(1, &ImageName); 
	ilBindImage(ImageName);
	if (!ilLoadImage((ILstring)image_path.c_str())) std::cout << "[-] Image couldn't be loaded" << std::endl;

	GLuint textureID;
	glGenTextures(1, &textureID); 
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE), ilGetData());
	glGenerateMipmap(GL_TEXTURE_2D);

	ilDeleteImages(1, &ImageName);
	glBindTexture(GL_TEXTURE_2D, 0);

	return textureID;
}

std::vector<MeshTexture> Model::LoadMaterialTexture(aiMaterial* mat, aiTextureType type, std::string type_name)
{
	std::vector<MeshTexture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString ai_str;
		mat->GetTexture(type, i, &ai_str);

		std::string filename = std::string(ai_str.C_Str());
		filename =  "res/models/" + filename;

		fs::file_status s = fs::file_status{};
		if (fs::status_known(s) ? fs::exists(s) : fs::exists(filename)) {
			std::cout << "[+] Texture Image " << filename << " found" << std::endl;
		}
		else {
			std::cout << "[!] Texture Image " << filename << " not found" << std::endl;
		}

		MeshTexture texture;
		texture.ID = this->LoadImageToTexture(filename);
		texture.Type = type_name;
		texture.Path = ai_str.C_Str();
		textures.push_back(texture);
	}
	return textures;
}

void Model::Init()
{
	for (int i = 0; i < this->meshes.size(); ++i) {
		meshes[i].Init();
	}
}

void Model::Render(Camera camera)
{
	for (int i = 0; i < this->meshes.size(); ++i) {
		meshes[i].Render(camera);
	}
	if (this->mesh_animation) {
		std::vector<aiMatrix4x4> transforms;
		// this->animation->BoneTransform(1.0f, transforms);

		//for (unsigned int i = 0; i < transforms.size(); i++) {
		//	glUniformMatrix4fv(animation->m_bone_location[i], 1, GL_TRUE, (const GLfloat*)&transforms[i]);
		//}
	}
}

void Model::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	for (int i = 0; i < this->meshes.size(); ++i) {
		this->meshes[i].Update(view_matrix, projection_matrix);
	}
}

void Model::Update(float angle, glm::vec3 pivot, glm::vec3 axis)
{
	for (int i = 0; i < this->meshes.size(); ++i) {
		this->meshes[i].Update(angle, pivot, axis);
	}
}

void Model::Clean()
{
	for (int i = 0; i < this->meshes.size(); ++i) {
		this->meshes[i].Clean();
	}
	this->meshes.clear();
}

Model::~Model() { this->Clean(); }