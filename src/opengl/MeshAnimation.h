#pragma once

#include <vector>
#include <map>
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include "VertexBuffer.h"
#include "Model.h"
#define NUM_BONES_PER_VERTEX 4

struct MeshBoneMatrix
{
	aiMatrix4x4 OffsetMatrix;
	aiMatrix4x4 FinalWorldTransform;
};

struct MeshVertexBoneData
{
	unsigned int IDs[NUM_BONES_PER_VERTEX];
	float Weights[NUM_BONES_PER_VERTEX];

	MeshVertexBoneData()
	{
		memset(IDs, 0, sizeof(IDs));
		memset(Weights, 0, sizeof(Weights));
	}

	void AddBoneData(unsigned int bone_id, float weight);
};

class Model;
class MeshAnimation {
private:
	std::vector<MeshVertexBoneData> bones_id_weights_per_vertex;
	VertexBuffer* vbo;
	static const unsigned int MAX_BONES = 100;

	const aiScene* scene;
	std::map<std::string, unsigned int> m_bone_mapping;
	unsigned int m_num_bones = 0;
	std::vector<MeshBoneMatrix> m_bone_matrices;
	aiMatrix4x4 m_global_inverse_transform;
	GLuint m_bone_location[MAX_BONES];

	unsigned int FindPosition(float p_animation_time, const aiNodeAnim* p_node_anim);
	unsigned int FindRotation(float p_animation_time, const aiNodeAnim* p_node_anim);
	unsigned int FindScaling(float p_animation_time, const aiNodeAnim* p_node_anim);
	const aiNodeAnim* FindNodeAnim(const aiAnimation* p_animation, const std::string p_node_name);
	// calculate transform matrix
	aiVector3D CalcInterpolatedPosition(float p_animation_time, const aiNodeAnim* p_node_anim);
	aiQuaternion CalcInterpolatedRotation(float p_animation_time, const aiNodeAnim* p_node_anim);
	aiVector3D CalcInterpolatedScaling(float p_animation_time, const aiNodeAnim* p_node_anim);

	void ReadNodeHierarchy(float p_animation_time, const aiNode* p_node, const aiMatrix4x4 parent_transform);
	void BoneTransform(double time_in_sec, std::vector<aiMatrix4x4>& transforms);
	void SetupAnimation();

public:
	MeshAnimation(Model& model);
	MeshAnimation(Model& model, std::vector<MeshVertexBoneData> bone_id_weights);
	virtual ~MeshAnimation();

	void AddBoneData(unsigned int bone_id, float weight);
	glm::mat4 AiToGlm(aiMatrix4x4 ai_matr);
	aiQuaternion NLerp(aiQuaternion a, aiQuaternion b, float blend);
};