#version 450

in vec3 fposition;

out vec4 fragmentColor;

void main()
{
	vec3 color = vec3(0.0f, 1.0f, 0.0f);
	fragmentColor = vec4(color, 1.0);
}