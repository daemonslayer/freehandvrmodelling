/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __CUBE_H__
#define __CUBE_H__
#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/vec3.hpp>
#include "Object.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "Program.h"

class Cube : public Object
{
private:
	VertexArray* vao;
	VertexBuffer* vbo[2];
	Program* program;
	std::vector<GLfloat> cube_points;
	std::vector<GLfloat> cube_normals;
	std::vector<glm::vec3> cube_positions;
	std::vector<float> cube_angles;
	std::vector<glm::vec3> cube_scale;
	glm::vec3 rotation_axis;

	glm::mat4 model_matrix;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;

public:
	Cube(glm::vec3 cube_position = glm::vec3(0.0f, 0.0f, 0.0f), float angle = 0.0f, glm::vec3 scale = glm::vec3(1.0f));
	Cube(std::vector<glm::vec3> cube_positions, std::vector<float> angles, std::vector<glm::vec3> scale_factors);
	virtual ~Cube() = default;

	void Init() override;
	void Render(Camera camera) override;
	void Update(glm::mat4 view_matrix, glm::mat4 projection_matrix) override;
	void Update(float angle, glm::vec3 pivot, glm::vec3 axis);
	void Clean() override;

	glm::vec3 GetRotationAxis();
	void SetRotationAxis(glm::vec3 rotation_axis);
	glm::mat4 GetModelMatrix();

};

#endif // __CUBE_H__