/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "Program.h"
#include <glm/gtc/type_ptr.hpp>


Program::Program() { }

Program::Program(const Shader& vertex)
{
	this->shader_programme = glCreateProgram();
	Attach(vertex);
	Link();
	glUseProgram(this->shader_programme);
}

Program::Program(const Shader& vertex, const Shader& fragment)
{
	this->shader_programme = glCreateProgram();
	Attach(vertex);
	Attach(fragment);
	Link();
	glUseProgram(this->shader_programme);
}

Program::Program(const Shader& vertex, const Shader& fragment, const Shader& geometry)
{
	this->shader_programme = glCreateProgram();
	Attach(vertex);
	Attach(fragment);
	Attach(geometry);
	Link();
	glUseProgram(this->shader_programme);
}

Program::operator GLuint() const { return this->shader_programme; }
const Program& Program::operator=(const Program& other)
{
	return *this;
}
void Program::Attach(const Shader& shader)
{
	glAttachShader(this->shader_programme, shader);
}
void Program::TransformFeedbackVaryings(const char** varyings, uint count)
{
	glTransformFeedbackVaryings(this->shader_programme, count, varyings, GL_INTERLEAVED_ATTRIBS);
}

void Program::Link()
{
	GLint res;

	glLinkProgram(this->shader_programme);
	glGetProgramiv(this->shader_programme, GL_LINK_STATUS, &res);
	int max_length = 2048;
	int actual_length = 0;
	char log[2048];
	glGetProgramInfoLog(this->shader_programme, max_length, &actual_length, log);
	printf("program info log for GL index %u:\n%s", this->shader_programme, log);
	if (res == GL_FALSE)
		throw LinkException(GetInfoLog());
}

std::string Program::GetInfoLog()
{
	GLint res;
	glGetProgramiv(this->shader_programme, GL_INFO_LOG_LENGTH, &res);

	if (res > 0)
	{
		std::string infoLog(res, 0);
		glGetProgramInfoLog(this->shader_programme, res, &res, &infoLog[0]);
		return infoLog;
	}
	else {
		return "";
	}
}

Program::Attribute Program::GetAttribute(const std::string& name)
	{ return glGetAttribLocation(this->shader_programme, name.c_str()); }
void Program::SetAttribute(const std::string& name, GLuint index)
	{ glBindAttribLocation(this->shader_programme, index, name.c_str()); }
Program::Uniform Program::GetUniform(const std::string& name)
	{ return glGetUniformLocation(this->shader_programme, name.c_str()); }
void Program::SetUniform(const Uniform& uniform, int value)
	{ glUniform1i(uniform, value); }
void Program::SetUniform(const Uniform& uniform, float value)
	{ glUniform1f(uniform, value); }
void Program::SetUniform(const Uniform& uniform, const glm::vec2& value) 
	{ glUniform2f(uniform, value.x, value.y); }
void Program::SetUniform(const Uniform& uniform, const glm::vec3& value)
	{ glUniform3f(uniform, value.x, value.y, value.z); }
void Program::SetUniform(const Uniform& uniform, const glm::vec4& value)
	{ glUniform4f(uniform, value.x, value.y, value.z, value.w); }
void Program::SetUniform(const Uniform& uniform, const float* values, uint count)
	{ glUniform1fv(uniform, count, values); }
void Program::SetUniform(const Uniform& uniform, const glm::vec2* values, uint count)
	{ glUniform2fv(uniform, count, (float*)values); }
void Program::SetUniform(const Uniform& uniform, const glm::vec3* values, uint count)
	{ glUniform3fv(uniform, count, (float*)values); }
void Program::SetUniform(const Uniform& uniform, const glm::vec4* values, uint count)
	{ glUniform4fv(uniform, count, (float*)values); }
void Program::SetUniform(const Uniform& uniform, const glm::mat3& value)
	{ glUniformMatrix3fv(uniform, 1, GL_FALSE, glm::value_ptr(value)); }
void Program::SetUniform(const Uniform& uniform, const glm::mat4& value)
	{ glUniformMatrix4fv(uniform, 1, GL_FALSE, glm::value_ptr(value)); }
void Program::SetUniformSampler2d(const UniformSampler2d& uniform_sampler2d, int location)
	{ glUniform1i(uniform_sampler2d, location); }