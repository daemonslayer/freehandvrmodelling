/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __BOUNDINGBOX_H__
#define __BOUNDINGBOX_H__

#include <glm/glm.hpp>
#include "RayCast.h"

class BoundingBox
{
private:
	glm::vec3 min_corner;
	glm::vec3 max_corner;

public:
	BoundingBox(glm::vec3 min_corner = glm::vec3(0.0f, 0.0f, 0.0f), 
		glm::vec3 max_corner = glm::vec3(1.0f, 1.0f, 1.0f));
	BoundingBox(Object* object);
	virtual ~BoundingBox();
	
	glm::vec3 GetCenter();
	void SetMinCorner(glm::vec3 min_corner);
	void SetMaxCorner(glm::vec3 max_corner);
	glm::vec3 GetMinCorner();
	glm::vec3 GetMaxCorner();

};

#endif // __BOUNDINGBOX_H__