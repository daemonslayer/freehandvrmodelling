/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "SwipeDetector.h"
#include <algorithm>
#include <iostream>

#define SWIPE_VELOCITY_THRESHOLD 400

SwipeDetector::SwipeDetector() :
	gesture_type(GESTURE_TYPE::SWIPE_GESTURE),
	swipe_velocity_threshold(SWIPE_VELOCITY_THRESHOLD) { }

void SwipeDetector::CheckLeapFrame(const LEAP_TRACKING_EVENT* frame)
{
	const LEAP_HAND* hand = &frame->pHands[0];
	float max = std::max(std::max(hand->palm.velocity.x, hand->palm.velocity.y), hand->palm.velocity.z);
	if (max > this->swipe_velocity_threshold)
		this->is_gesture_active = true;
	else this->is_gesture_active = false;
}

GESTURE_TYPE SwipeDetector::Type() { return this->gesture_type; }
void SwipeDetector::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

bool SwipeDetector::IsGestureActive(const LEAP_TRACKING_EVENT* frame)
{
	if (frame->nHands <= 0) return false;
	this->CheckLeapFrame(frame);
	if (this->is_gesture_active) {
		std::cout << "Swipe Gesture Active" << std::endl;
		return true;
	}
	else {
		return false;
	}
}

void SwipeDetector::RegisterGestureListener(IGestureListener* listener)
{

}

void SwipeDetector::RemoveGestureListener(IGestureListener* listener)
{

}

void SwipeDetector::NotifyGestureListener(bool is_active)
{

}
