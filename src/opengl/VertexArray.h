/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __VERTEXARRAY_H__
#define __VERTEXARRAY_H__
#pragma once

#include <stdint.h>
#include <GL/glew.h>
#include "Platform.h"
#include "VertexBuffer.h"

class VertexArray
{
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef GLint Attribute;
typedef GLint Uniform;

private:
	GLuint vao;

public:
	VertexArray();
	VertexArray(const VertexArray& varray);
	virtual ~VertexArray();

	operator GLuint() const;
	void BindAttribute(const Attribute& attribute, const VertexBuffer& buffer, int Type, uint count, uint stride, intptr_t offset);
	void BindElements(const VertexBuffer& elements);
	void BindTransformFeedback(uint index, const VertexBuffer& buffer);
};

#endif  // __VERTEXARRAY_H__