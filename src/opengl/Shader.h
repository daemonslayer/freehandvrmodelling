/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __SHADER_H__
#define __SHADER_H__
#pragma once

#include <string>
#include <exception>
#include <GL/glew.h>

class CompileException : public std::exception
{
public:
	CompileException(const std::string& str) throw() : infoLog(str) {}
	~CompileException() throw() {}

	virtual const char* what() const throw()
	{
		return infoLog.c_str();
	}

private:
	std::string infoLog;
};

enum ShaderType {
	Vertex = GL_VERTEX_SHADER,
	Fragment = GL_FRAGMENT_SHADER,
	Geometry = GL_GEOMETRY_SHADER,
};

class Shader {
private:
    GLuint shader;

public:
	Shader(ShaderType type);
	Shader(ShaderType type, const std::string& url);
	virtual ~Shader() = default;

	operator GLuint() const;
	const Shader& operator=(const Shader& other);

	void Source(const std::string& code);
	void Compile();

	std::string GetInfoLog();
};

#endif  // __SHADER_H__