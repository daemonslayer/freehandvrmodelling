#include "Raycast.h"

Raycast::Raycast() { }
glm::vec3 Raycast::GetOrigin() { return this->origin; }
void Raycast::SetOrigin(glm::vec3 origin) { this->origin = origin; }
glm::vec3 Raycast::GetDirection() { return this->direction; }
void Raycast::SetDirection(glm::vec3 direction) { this->direction = direction; }