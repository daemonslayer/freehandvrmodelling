#include "Surface.h"


Surface::Surface(glm::vec3 position, float angle, glm::vec3 scale) :
	position(position),
	angle(angle),
	scale(scale)
{
	/*Shader s_vert(ShaderType::Vertex, "res/shaders/surface_vs.glsl");
	Shader s_geo(ShaderType::Geometry, "res/shaders/surface_gs.glsl");
	Shader s_frag(ShaderType::Fragment, "res/shaders/surface_fs.glsl");
	program = new Program(s_vert, s_frag, s_geo);*/

	Shader s_vert(ShaderType::Vertex, "res/shaders/hands_vs.glsl");
	Shader s_frag(ShaderType::Fragment, "res/shaders/hands_fs.glsl");
	program = new Program(s_vert, s_frag);

	vbo[0] = new VertexBuffer(&frame_points[0], frame_points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	// vbo[1] = new VertexBuffer(&frame_normals[0], frame_normals.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vao = new VertexArray();
	vao->BindAttribute(program->GetAttribute("position"), *vbo[0], GL_FLOAT, 3, 0, 0);
	// vao->BindAttribute(program->GetAttribute("normal"), *vbo[1], GL_FLOAT, 3, 0, 0);
	this->Init();
}

void Surface::Init()
{
	this->model_matrix = glm::scale(this->model_matrix, this->scale);
	this->model_matrix = glm::rotate(this->model_matrix, this->angle, glm::vec3(1.0f));
	this->model_matrix = glm::translate(this->model_matrix, this->position);
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
	glUseProgram(0);
}

void Surface::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());

	if (!IsConnected) exit(0);
	const LEAP_TRACKING_EVENT* frame = GetFrame();
	/*if (frame->nHands > 0) {
		for (int i = 0; i < frame->nHands; ++i) {
			const LEAP_HAND* hand = &frame->pHands[i];
			for (int f = 0; f < 5; ++f)
			{
				LEAP_DIGIT finger = hand->digits[f];
				for (int b = 0; b < 4; ++b)
				{
					LEAP_BONE bone = finger.bones[b];
					this->frame_points.push_back(bone.next_joint.x);
					this->frame_points.push_back(bone.next_joint.y);
					this->frame_points.push_back(bone.next_joint.z);
				}
			}
		}
	}*/
	std::vector<GLfloat> points = sampler.SampleCurrentFrame();
	this->frame_points.insert(frame_points.end(), points.begin(), points.end());
	/*this->surface.AddLeapFrame(this->frame_points);
	surface.GenerateNormals();
	this->frame_projected_points = surface.GetProjectedPoints();
	this->frame_normals = surface.GetNormalPoints();*/

	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	vbo[0]->Data(&points[0], points.size() * sizeof(GLfloat), GL_DYNAMIC_DRAW);
	// vbo[1]->Data(&frame_normals[0], frame_normals.size() * sizeof(GLfloat), GL_DYNAMIC_DRAW);
	glBindVertexArray(*this->vao);
	glDrawArrays(GL_POINTS, 0, points.size() / 3);
	glUseProgram(0);
}

void Surface::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;

	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void Surface::Clean()
{
	this->frame_points.clear();
	this->frame_normals.clear();
}

Surface::~Surface() { }