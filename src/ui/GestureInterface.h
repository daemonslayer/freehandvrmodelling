#pragma once
#include <vector>
#include "LeapConnection.h"
#include "Interface.h"
#include "GestureListener.h"

class GestureInterface : public Interface
{
private:
	MODE current_mode;
	GestureListener gesture_listener;

	const std::vector<GESTURE_TYPE> GesturesInCurrentMode();
	void DrawModeRules();
	void EditModeRules();
	void UserInterfaceModeRules();
	void ViewModeRules();

public:
	GestureInterface(MODE mode = MODE::VIEW);
	virtual ~GestureInterface() = default;

	MODE CurrentMode() override;
	const std::vector<ACTIONS> PossibleActionsInCurrentMode() override;
	bool ChangeMode(MODE currentMode, MODE nextMode);
	void PollGestureDetectors();
};

