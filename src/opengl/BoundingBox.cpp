#include "BoundingBox.h"

BoundingBox::BoundingBox(glm::vec3 min_corner, glm::vec3 max_corner) : 
	min_corner(min_corner),
	max_corner(max_corner) { }
BoundingBox::BoundingBox(Object* object)
{
	
}

glm::vec3 BoundingBox::GetCenter() { return (min_corner + max_corner) / glm::vec3(2.0f, 2.0f, 2.0f); }
void BoundingBox::SetMinCorner(glm::vec3 min_corner) { this->min_corner = min_corner; }
void BoundingBox::SetMaxCorner(glm::vec3 max_corner) { this->max_corner = max_corner; }
glm::vec3 BoundingBox::GetMinCorner() { return this->min_corner; }
glm::vec3 BoundingBox::GetMaxCorner() { return this->max_corner; }
BoundingBox::~BoundingBox() { }