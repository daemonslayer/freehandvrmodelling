#include "Pointer.h"
#include <iostream>
#include <LeapC.h>
#include "LeapConnection.h"


Pointer::Pointer()
{
	Shader r_vert(ShaderType::Vertex, "res/shaders/pointer_vs.glsl");
	Shader r_frag(ShaderType::Fragment, "res/shaders/pointer_fs.glsl");
	program = new Program(r_vert, r_frag);
	
	vbo = new VertexBuffer(&ray_points[0], ray_points.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
	vao = new VertexArray();
	vao->BindAttribute(program->GetAttribute("position"), *vbo, GL_FLOAT, 3, 0, 0);
	this->Init();
}

void Pointer::Init()
{
	// this->model_matrix = glm::rotate(this->model_matrix, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	this->model_matrix = glm::translate(this->model_matrix, glm::vec3(0.0f, -100.0f, -180.0f));
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("modelMatrix"), this->view_matrix);
	glUseProgram(0);
}

void Pointer::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());

	if (!IsConnected) exit(0);
	const LEAP_TRACKING_EVENT* frame = GetFrame();
	const LEAP_HAND* hand = &frame->pHands[0];
	const LEAP_PALM* palm = &hand->palm;
	glm::vec3 indexfinger_direction = glm::normalize(glm::vec3(
		hand->index.distal.next_joint.x - hand->index.intermediate.prev_joint.x,
		hand->index.distal.next_joint.y - hand->index.intermediate.prev_joint.y,
		hand->index.distal.next_joint.z - hand->index.intermediate.prev_joint.z));

	glm::vec3 p0 = glm::vec3(hand->index.distal.next_joint.x, hand->index.distal.next_joint.y, hand->index.distal.next_joint.z);
	glm::vec3 p1 = p0 + indexfinger_direction * glm::vec3(150.0f);
		
	this->ray_points.clear();
	this->ray_points.push_back(p0);
	this->ray_points.push_back(p1);
		
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	vbo->Data(&ray_points[0], ray_points.size() * sizeof(glm::vec3), GL_DYNAMIC_DRAW);
	glBindVertexArray(*this->vao);
	glDrawArrays(GL_LINES, 0, 2);
	glUseProgram(0);
}

void Pointer::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;

	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void Pointer::Update(glm::vec3 position)
{
	if (this->ray_points.size() > 1) {
		this->ray_points[ray_points.size() - 1] = position;
	}
	else if(this->ray_points.size() == 1){
		this->ray_points[0] = position;
	}
	else {
		this->ray_points.push_back(position);
	}

	glUseProgram(*(this->program));
	vbo->Data(&ray_points[0], ray_points.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
	glUseProgram(0);
}

void Pointer::Clean()
{
	this->ray_points.clear();
	this->direction = glm::vec3(0.0f);
	this->origin = glm::vec3(0.0f);
}

glm::vec3 Pointer::GetDirection() { return this->direction; }
glm::vec3 Pointer::GetOrigin() { return this->origin; }

Pointer::~Pointer() { this->Clean(); }
