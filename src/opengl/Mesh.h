#pragma once
#include "Object.h"
#include <string>
#include <vector>
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "ElementBuffer.h"
#include "Program.h"

struct MeshVertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangent;
	glm::vec3 Bitangent;
};

struct MeshTexture
{
	unsigned int ID;
	std::string Type;
	std::string Path;
};

class Mesh : public Object
{
private:
	std::vector<MeshVertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<MeshTexture> textures;
	VertexArray* vao;
	VertexBuffer* vbo;
	ElementBuffer* ebo;
	Program* program;

	glm::mat4 model_matrix;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;

	void SetupMesh();

public:
	Mesh();
	Mesh(std::vector<MeshVertex> vertices, std::vector<unsigned int> indices, std::vector<MeshTexture> textures);
	virtual ~Mesh();

	void Init() override;
	void Render(Camera camera) override;
	void Update(glm::mat4 view_matrix, glm::mat4 projection_matrix) override;
	void Update(float angle, glm::vec3 pivot, glm::vec3 axis);
	void Clean() override;

};

