#version 450

layout (points) in;
layout (triangle_strip, max_vertices=4) out;

in VertexData
{
  vec3  normal;
  float radius;
} VertexIn[];

out FragmentData 
{
  vec2 texcoord;
  vec3 normal;
  vec3 position;
} VertexOut;

void main() {
  vec3 basis_u, basis_v;
  vec3 basis_w = normalize(VertexIn[0].normal);
  vec3 second_vector;
  
  if (basis_w.x < basis_w.y)
    if (basis_w.x < basis_w.z) 
      second_vector = vec3(1, basis_w.yz);
    else 
      second_vector = vec3(basis_w.xy, 1.0);
  else 
    if (basis_w.y < basis_w.z) 
      second_vector = vec3(basis_w.x, 1.0, basis_w.z);
    else 
      second_vector = vec3(basis_w.xy, 1.0);

  second_vector = normalize(second_vector);
  basis_u = cross(second_vector, basis_w);
  basis_v = cross(basis_w, basis_u);

  vec3 pointCenter = gl_in[0].gl_Position.xyz;
  vec4 a = vec4(pointCenter + VertexIn[0].radius * (-basis_u - basis_v), gl_in[0].gl_Position.w);
  vec4 b = vec4(pointCenter + VertexIn[0].radius * (-basis_u + basis_v), gl_in[0].gl_Position.w);
  vec4 c = vec4(pointCenter + VertexIn[0].radius * (+basis_u + basis_v), gl_in[0].gl_Position.w);
  vec4 d = vec4(pointCenter + VertexIn[0].radius * (+basis_u - basis_v), gl_in[0].gl_Position.w);

  gl_Position = b;
  VertexOut.texcoord = vec2(-1.0, 1.0);
  VertexOut.normal = basis_w;
  VertexOut.position = vec3(gl_in[0].gl_Position.x, gl_in[0].gl_Position.y, gl_in[0].gl_Position.z);
  EmitVertex();
  gl_Position = a;
  VertexOut.texcoord = vec2(-1.0, -1.0);
  VertexOut.normal = basis_w;
  VertexOut.position = vec3(gl_in[0].gl_Position.x, gl_in[0].gl_Position.y, gl_in[0].gl_Position.z);
  EmitVertex();
  gl_Position = c;
  VertexOut.texcoord = vec2(1.0, 1.0);
  VertexOut.normal = basis_w;
  VertexOut.position = vec3(gl_in[0].gl_Position.x, gl_in[0].gl_Position.y, gl_in[0].gl_Position.z);
  EmitVertex();
  gl_Position = d;
  VertexOut.texcoord = vec2(1.0, -1.0);
  VertexOut.normal = basis_w;
  VertexOut.position = vec3(gl_in[0].gl_Position.x, gl_in[0].gl_Position.y, gl_in[0].gl_Position.z);
  EmitVertex();
  EndPrimitive();
}