/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "Cube.h"
#include <iostream>

Cube::Cube(glm::vec3 cube_position, float angle, glm::vec3 scale) :
	rotation_axis(glm::vec3(1.0f, 1.0f, 1.0f))
{
	this->cube_points = {
		-0.5f, -0.5f, -0.5f, //Left face 1
		-0.5f, -0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f, -0.5f, -0.5f, //Left face 2
		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f, //Right face 1
		 0.5f, -0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f, //Right face 2
		 0.5f,  0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f, //Bottom face 1
		-0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f,  0.5f, //Bottom face 2
		-0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f, //Top face 1
		 0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f, //Top face 2
		-0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f, -0.5f, // Back face 1
		-0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f, // Back face 2
		 0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f,  0.5f, //Front face 1
		-0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f, //Front face 2
		-0.5f,  0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f
	};
	this->cube_normals = {
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
	};
	this->cube_positions.push_back(cube_position);
	this->cube_angles.push_back(angle);
	this->cube_scale.push_back(scale);

	Shader c_vert(ShaderType::Vertex, "res/shaders/cube_vs.glsl");
	Shader c_frag(ShaderType::Fragment, "res/shaders/cube_fs.glsl");
	program = new Program(c_vert, c_frag);

	vbo[0] = new VertexBuffer(&cube_points[0], cube_points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vbo[1] = new VertexBuffer(&cube_normals[0], cube_normals.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vao = new VertexArray();
	vao->BindAttribute(program->GetAttribute("position"), *vbo[0], GL_FLOAT, 3, 0, 0);
	vao->BindAttribute(program->GetAttribute("normal"), *vbo[1], GL_FLOAT, 3, 0, 0);
	this->Init();
}

Cube::Cube(std::vector<glm::vec3> cube_positions, std::vector<float> angles, std::vector<glm::vec3> scale_factors) :
	rotation_axis(glm::vec3(1.0f, 1.0f, 1.0f))
{
	this->cube_points = {
		-0.5f, -0.5f, -0.5f, //Left face 1
		-0.5f, -0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f, -0.5f, -0.5f, //Left face 2
		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f, //Right face 1
		 0.5f, -0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f, //Right face 2
		 0.5f,  0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f, //Bottom face 1
		-0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f,  0.5f, //Bottom face 2
		-0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f, //Top face 1
		 0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f, //Top face 2
		-0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f, -0.5f, // Back face 1
		-0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f, // Back face 2
		 0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f,  0.5f, //Front face 1
		-0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f, //Front face 2
		-0.5f,  0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f
	};
	this->cube_normals = {
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		-1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 1,  0,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0, -1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  1,  0,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0, -1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
		 0,  0,  1,
	};
	this->cube_positions = cube_positions;
	this->cube_angles = cube_angles;
	this->cube_scale = scale_factors;

	Shader c_vert(ShaderType::Vertex, "res/shaders/cube_vs.glsl");
	Shader c_frag(ShaderType::Fragment, "res/shaders/cube_fs.glsl");
	program = new Program(c_vert, c_frag);

	vbo[0] = new VertexBuffer(&cube_points[0], cube_points.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vbo[1] = new VertexBuffer(&cube_normals[0], cube_normals.size() * sizeof(GLfloat), GL_STATIC_DRAW);
	vao = new VertexArray();
	vao->BindAttribute(program->GetAttribute("position"), *vbo[0], GL_FLOAT, 3, 0, 0);
	vao->BindAttribute(program->GetAttribute("normal"), *vbo[1], GL_FLOAT, 3, 0, 0);
	this->Init();
}

void Cube::Init()
{
	for (int i = 0; i < this->cube_positions.size(); ++i) {
		this->model_matrix = glm::mat4(1.0f);
		this->model_matrix = glm::scale(this->model_matrix, cube_scale[i]);
		this->model_matrix = glm::rotate(this->model_matrix, this->cube_angles[i], glm::vec3(1.0f));
		this->model_matrix = glm::translate(this->model_matrix, this->cube_positions[i]);
		glUseProgram(*(this->program));
		glBindVertexArray(*vao);
		program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
		glUseProgram(0);
	}
}

void Cube::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glBindVertexArray(*vao);
	glDrawArrays(GL_TRIANGLES, 0, this->cube_points.size() / 3);
	glUseProgram(0);
}

void Cube::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;

	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void Cube::Update(float angle_in_radians, glm::vec3 pivot, glm::vec3 axis)
{
	glm::quat quaternion_rot = glm::angleAxis(angle_in_radians, axis);
	glm::mat4 rotation_mat = glm::toMat4(quaternion_rot);
	glm::mat4 pivot_mat = glm::translate(glm::mat4(1.0f), 
		cube_positions[0]) * rotation_mat * glm::translate(glm::mat4(1.0f), -cube_positions[0]);
	this->model_matrix = pivot_mat * this->model_matrix;
	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
	glUseProgram(0);
}

void Cube::Clean()
{
	this->cube_points.clear();
	this->cube_normals.clear();
	this->cube_positions.clear();
	this->cube_angles.clear();
	this->model_matrix = glm::mat4(1.0f);
	this->view_matrix = glm::mat4(1.0f);
	this->projection_matrix = glm::mat4(1.0f);
}

glm::vec3 Cube::GetRotationAxis() { return this->rotation_axis; }
void Cube::SetRotationAxis(glm::vec3 rotation_axis) { this->rotation_axis = rotation_axis; }
glm::mat4 Cube::GetModelMatrix() { return this->model_matrix; }