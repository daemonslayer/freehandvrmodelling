#pragma once

#include <glm/glm.hpp>
#include "Raycast.h"
#include "Object.h"
#include "BoundingBox.h"
#include "BoundingSphere.h"
#include "Mesh.h"

struct RayHitInfo
{
	bool hit;
	float distance;
	glm::vec3 position;
	glm::vec3 normal;
};

class Collision
{
public:
	Collision();
	virtual ~Collision();

	RayHitInfo RayTriangleCollision(Raycast& ray, glm::vec3 points) const;
	RayHitInfo RayObjectCollision(Raycast& ray, Object* object) const;
	RayHitInfo RayCubeCollision(Raycast& ray, BoundingBox& cube) const;
	RayHitInfo RaySphereCollision(Raycast& ray, BoundingSphere& sphere) const;
	RayHitInfo RayPlaneCollision(Raycast& ray, float plane_height) const;
	RayHitInfo RayMeshCollision(const Raycast& ray, const Mesh& mesh);

};

