#pragma once

class Scene
{
private:
	virtual bool OnInitialize() = 0;
	virtual bool OnDestroy() = 0;
	virtual bool OnUpdate() = 0;
	virtual bool OnGui() = 0;
	virtual bool OnDisplay() = 0;
	virtual bool GetCameraOverride();
	virtual bool GetSceneBounds() = 0;


public:
	Scene();
	virtual ~Scene();

	int Run(int argc, const char** argv, const char* version, const char* title);
};