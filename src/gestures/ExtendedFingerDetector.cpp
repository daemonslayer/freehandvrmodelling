/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "ExtendedFingerDetector.h"
#include <iostream>


ExtendedFingerDetector::ExtendedFingerDetector() : 
	gesture_type(GESTURE_TYPE::EXTENDEDFINGER_GESTURE) { }

void ExtendedFingerDetector::CheckLeapFrame(const LEAP_TRACKING_EVENT* frame) 
{
	/*if (this->_HandType == HAND_TYPE::NONE) {
		std::cout << "[-] ExtendedFinger Gesture requires atleast one hand" << std::endl;
		exit(0);
	}*/
	bool FingersExtended = true;
	const LEAP_HAND* hand = &frame->pHands[0];
	for (int f = 0; f < 5; ++f) {
		LEAP_DIGIT finger = hand->digits[f];
		FingersExtended = FingersExtended && finger.is_extended;
	}

	if (FingersExtended) this->is_gesture_active = true;
	else this->is_gesture_active = false;
}

GESTURE_TYPE ExtendedFingerDetector::Type() { return this->gesture_type; }
void ExtendedFingerDetector::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

bool ExtendedFingerDetector::IsGestureActive(const LEAP_TRACKING_EVENT* frame) 
{
	if (frame->nHands <= 0) return false;
	this->CheckLeapFrame(frame);
	if (this->is_gesture_active) {
		return true;
	}
	else {
		return false;
	}
}

void ExtendedFingerDetector::RegisterGestureListener(IGestureListener* listener) 
{

}

void ExtendedFingerDetector::RemoveGestureListener(IGestureListener* listener) 
{

}

void ExtendedFingerDetector::NotifyGestureListener(bool is_active) 
{

}
