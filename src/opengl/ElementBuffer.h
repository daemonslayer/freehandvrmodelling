/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __ELEMENTBUFFER_H__
#define __ELEMENTBUFFER_H__
#pragma once

#include <GL/glew.h>

class ElementBuffer
{
private:
	GLuint ebo;

public:
	ElementBuffer();
	ElementBuffer(const ElementBuffer& ebuffer);
	ElementBuffer(const void* data, size_t length, int BufferUsage);
	virtual ~ElementBuffer();
	
	operator GLuint() const;
	void Data(const void* data, size_t length, int BufferUsage);
	void SubData(const void* data, size_t offset, size_t length);
	void GetSubData(void* data, size_t offset, size_t length);
};

#endif