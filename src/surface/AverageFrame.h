#pragma once
#include <vector>
#include <fstream>
#include <glm/glm.hpp>
#include "LeapC.h"
#include "LeapConnection.h"

#define POOL_SIZE 5

struct AverageFrame {
private:
	std::vector<const LEAP_TRACKING_EVENT*> frames;
	LEAP_TRACKING_EVENT* avg_frame;

public:
	glm::vec3 pivot;
	glm::vec3 axis;
	float angle;
	glm::vec3 hands_distance;

	AverageFrame();
	~AverageFrame() = default;
	const LEAP_TRACKING_EVENT* GetAverageFrame();
	void Clear();
	bool IsFull();
	bool IsEmpty();
	int Size();
	void AddFrame(const LEAP_TRACKING_EVENT* frame);
	void AddCurrentFrame();
	const LEAP_TRACKING_EVENT* GetFirstFrame();
	const LEAP_TRACKING_EVENT* operator[] (int);
	operator const LEAP_TRACKING_EVENT*();
	void RemoveFrame(int index);
	void RemoveFirstFrame();
	void RemoveLastFrame();
};