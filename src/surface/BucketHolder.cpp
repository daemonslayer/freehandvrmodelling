/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include <iostream>
#include "BucketHolder.h"

GLuint BucketHolder::vao_bucket_holder = -1;
int BucketHolder::size_of_vector = -1;
bool BucketHolder::sizeInitialized = false;
void BucketHolder::SetSizeOfPoints(int size) {
    if (!sizeInitialized)
        BucketHolder::size_of_vector = size;
    else
        std::cout << "[-] Cannot change size of points once initialized" << std::endl;
}

void BucketHolder::SetupVertexArray() {
    glGenVertexArrays(1, &BucketHolder::vao_bucket_holder);
    glBindVertexArray(BucketHolder::vao_bucket_holder);
}
void BucketHolder::PushVertexBuffersToArray() {
    for(int i=0; i<this->vbo_buckets.size(); ++i) {
        glBindBuffer(GL_ARRAY_BUFFER, this->vbo_buckets[i]);
        glVertexAttribPointer(i, this->size_of_vector, GL_FLOAT, GL_FALSE, 0, NULL);
    }
}

void BucketHolder::SetTopBucket(int bucket_id) {
    for(int i=0; i < buckets.size(); ++i) {
        if (this->buckets[i].get().GetBucketID() == bucket_id) {
            this->topBucket = &this->buckets[i].get();
            break;
        }
    }
}

void BucketHolder::AddBucket() {
    Bucket bucket(this->size_of_vector);
    this->buckets.push_back(bucket);
    this->topBucket = &bucket;
}

BucketHolder::BucketHolder() { this->total_points = 0; }
BucketHolder::BucketHolder(int size_of_vector) { 
    this->size_of_vector = size_of_vector; 
    this->total_points = 0;
}

Bucket* BucketHolder::GetTopBucket() { return topBucket; }
int BucketHolder::GetTopBucketID() { return topBucket->GetBucketID(); }
GLuint BucketHolder::GetVertexArray() { return this->vao_bucket_holder; }
uint64_t BucketHolder::PointsCount() { return this->total_points; }
int BucketHolder::GetSizeOfPoints() { return this->size_of_vector; }

void BucketHolder::AddPoints(std::vector<float> points) {
    this->total_points += points.size();
    if (this->topBucket->IfSpacePresent()) {
        this->topBucket->AddPointsToBucket(points);
    } else {
        this->AddBucket();
        this->topBucket->AddPointsToBucket(points);
    }
}

void BucketHolder::SetupVertexBuffers() {
    this->vbo_buckets.clear();
    for(int i=0; i<this->buckets.size(); ++i) {
        vbo_buckets.push_back(this->buckets[i].get().SetupVertexBuffer());
    }
}

void BucketHolder::DisplayPoints() {
    this->SetupVertexArray();
    this->SetupVertexBuffers();
    this->PushVertexBuffersToArray();    
}