/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "GestureListener.h"



GestureListener::GestureListener() { }

bool GestureListener::EnableGesture(GESTURE_TYPE gesture_type)
{
	switch (gesture_type)
	{
	case GESTURE_TYPE::HOLD_GESTURE:
	{
		HoldGestureDetector* holdgestureDetector = new HoldGestureDetector();
		this->gesture_detectors.push_back(holdgestureDetector);
		break;
	}

	case GESTURE_TYPE::EXTENDEDFINGER_GESTURE:
	{
		ExtendedFingerDetector* extendedfingerDetector = new ExtendedFingerDetector();
		this->gesture_detectors.push_back(extendedfingerDetector);
		break;
	}

	case GESTURE_TYPE::PINCH_GESTURE:
	{
		PinchDetector* pinchDetector = new PinchDetector();
		this->gesture_detectors.push_back(pinchDetector);
		break;
	}

	case GESTURE_TYPE::SWIPE_GESTURE:
	{
		SwipeDetector* swipeDetector = new SwipeDetector();
		this->gesture_detectors.push_back(swipeDetector);
		break;
	}

	case GESTURE_TYPE::FINGERPOINT_GESTURE:
	{
		FingerPointDetector* fingerpointDetector = new FingerPointDetector();
		this->gesture_detectors.push_back(fingerpointDetector);
		break;
	}

	case GESTURE_TYPE::PINCHSWIPE_GESTURE:
	{
		PinchDrawDetector* pinchdrawDetector = new PinchDrawDetector();
		this->gesture_detectors.push_back(pinchdrawDetector);
		break;
	}

	case GESTURE_TYPE::DOUBLETWOHANDHOLD_GESTURE:
	{
		DoubleTwoHandHoldDetector* doubletwohandholdDetector = new DoubleTwoHandHoldDetector();
		this->gesture_detectors.push_back(doubletwohandholdDetector);
		break;
	}

	case GESTURE_TYPE::FACINGHANDS_GESTURE:
	{
		FacingHandsDetector* facinghandsDetector = new FacingHandsDetector();
		this->gesture_detectors.push_back(facinghandsDetector);
		break;
	}

	default:
		break;
	}
	return true;
}

GESTURE_TYPE GestureListener::CheckFrameForGesture()
{
	if (!IsConnected) exit(0);
	const LEAP_TRACKING_EVENT* frame = GetFrame();
	for (int i = 0; i < this->gesture_detectors.size(); ++i) {
		if (this->gesture_detectors[i]->IsGestureActive(frame)) {
			// return this->GestureActive(gesture_detectors[i]->Type());
			return gesture_detectors[i]->Type();
		}
	}
	return GESTURE_TYPE::NONE;
}

GESTURE_TYPE GestureListener::GestureActive(GESTURE_TYPE gesture_type)
{
	if (this->last_active_gesture == gesture_type) {
		return GESTURE_TYPE::NONE;
	}
	else {
		this->last_active_gesture = gesture_type;
		return gesture_type;
	}
}

bool GestureListener::DisableGesture(GESTURE_TYPE gesture_type)
{
	for (int i = 0; i < this->gesture_detectors.size(); ++i) {
		if (this->gesture_detectors[i]->Type() == gesture_type) {
			this->gesture_detectors.erase(this->gesture_detectors.begin() + i);
			this->gesture_listeners.erase(this->gesture_listeners.begin() + i);
		}
	}
	return true;
}

bool GestureListener::DisableAllGestures()
{
	this->gesture_detectors.clear();
	this->gesture_listeners.clear();
	this->last_active_gesture = GESTURE_TYPE::NONE;
	return true;
}