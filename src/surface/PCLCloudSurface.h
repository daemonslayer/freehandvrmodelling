/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __PCLCLOUDSURFACE_H__
#define __PCLCLOUDSURFACE_H__
#pragma once

// TODO: Add error handling, clear after every frame processed, add checks for 
// if no points entered etc.

#include <vector>
#include <glm/glm.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_types.h>
#include <pcl/surface/mls.h>

class PCLCloudSurface {
private:
    pcl::PointCloud<pcl::PointXYZ>::Ptr points_cloud;
    pcl::PointCloud<pcl::PointXYZ>::Ptr projected_points_cloud;
    pcl::PointCloud<pcl::PointNormal>::Ptr normals_cloud;
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree;
    pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;

public:
    PCLCloudSurface();
    virtual ~PCLCloudSurface() = default;

    void AddLeapFrame(std::vector<float> points);
    void PerformUpsampling();
    void GenerateNormals();
    std::vector<float>& GetProjectedPoints();
    std::vector<float>& GetNormalPoints();
	// std::vector<float>& GetKNNAverageDistance();
	glm::vec3 ProjectPointToMLSSurface(glm::vec3 point);
	std::vector<glm::vec3>& ProjectPointsToMLSSurface(std::vector<glm::vec3>& points);
};

#endif  // __PCLCLOUDSURFACE_H__