/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "VertexBuffer.h"

VertexBuffer::VertexBuffer() { }
VertexBuffer::VertexBuffer(const VertexBuffer& vbuffer) { }

VertexBuffer::VertexBuffer(const void* data, size_t length, int BufferUsage)
{
	glGenBuffers(1, &this->vbo);
	Data(data, length, BufferUsage);
}

VertexBuffer::operator GLuint() const { return this->vbo; }

void VertexBuffer::Data(const void* data, size_t length, int BufferUsage)
{
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glBufferData(GL_ARRAY_BUFFER, length, data, BufferUsage);
}

void VertexBuffer::SubData(const void* data, size_t offset, size_t length)
{
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glBufferSubData(GL_ARRAY_BUFFER, offset, length, data);
}

void VertexBuffer::GetSubData(void* data, size_t offset, size_t length)
{
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glGetBufferSubData(GL_ARRAY_BUFFER, offset, length, data);
}

VertexBuffer::~VertexBuffer() { }
