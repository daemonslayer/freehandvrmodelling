/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#pragma once

#include <vector>
#include "IGestureDetectorLogicGate.h"
#include "IGestureListener.h"

class TwoHandSwipeDetector :
	public IGestureDetectorLogicGate
{
private:
	GESTURE_TYPE gesture_type;
	HAND_TYPE hand_type;
	bool is_gesture_active;
	std::vector<IGestureListener*> gesture_listeners;

	void CheckLeapFrame(const LEAP_TRACKING_EVENT* frame);
	double SquaredDistance(LEAP_VECTOR a, LEAP_VECTOR b);
	void NewFrameUpdate(const LEAP_HAND* hand);
	void ChangeState(bool ShouldBeActive);

public:
	TwoHandSwipeDetector();
	virtual ~TwoHandSwipeDetector() = default;

	GESTURE_TYPE Type() override;
	void RegisterGestureListener(IGestureListener* listener) override;
	void RemoveGestureListener(IGestureListener* listener) override;
	void NotifyGestureListener(bool is_active) override;

};

