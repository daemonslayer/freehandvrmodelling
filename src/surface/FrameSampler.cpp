#include "FrameSampler.h"


FrameSampler::FrameSampler() { }

std::vector<GLfloat> FrameSampler::SampleCurrentFrame()
{
	if (!IsConnected) exit(0);
	const LEAP_TRACKING_EVENT* frame = GetFrame();
	this->frame_points.clear();
	this->SampleFrame(frame);
	return this->frame_points;
}

float FrameSampler::TriangleArea(std::vector<GLfloat> points)
{
	float area;
	
	glm::vec3 e1 = glm::vec3(
		points[3] - points[0],
		points[4] - points[1],
		points[5] - points[2]);
	glm::vec3 e2 = glm::vec3(
		points[6] - points[0],
		points[7] - points[1],
		points[8] - points[2]);
	glm::vec3 e3 = glm::cross(e1, e2);
	area = 0.5 * std::sqrt(glm::dot(e3, e3));
	
	return area;
}

void FrameSampler::ParseFingers(const LEAP_DIGIT* finger_a, const LEAP_DIGIT* finger_b)
{
	for (int b = 0; b < 3; ++b) {
		const LEAP_BONE* bone_a = &finger_a->bones[b];
		const LEAP_BONE* bone_a_next = &finger_a->bones[b + 1];
		const LEAP_BONE* bone_b = &finger_b->bones[b];
		const LEAP_BONE* bone_b_next = &finger_b->bones[b + 1];
		std::vector<GLfloat> points;
		
		points.push_back(bone_a->next_joint.x);
		points.push_back(bone_a->next_joint.y);
		points.push_back(bone_a->next_joint.z);
		points.push_back(bone_a_next->next_joint.x);
		points.push_back(bone_a_next->next_joint.y);
		points.push_back(bone_a_next->next_joint.z);
		points.push_back(bone_b->next_joint.x);
		points.push_back(bone_b->next_joint.y);
		points.push_back(bone_b->next_joint.z);
		
		// std::vector<GLfloat> sampled_points_a = this->BarycentricSampling(points, 10);
		std::vector<GLfloat> sampled_points_a = this->PoissonSampling(points);
		frame_points.insert(frame_points.end(), sampled_points_a.begin(), sampled_points_a.end());
		points.clear();

		points.push_back(bone_b->next_joint.x);
		points.push_back(bone_b->next_joint.y);
		points.push_back(bone_b->next_joint.z);
		points.push_back(bone_a_next->next_joint.x);
		points.push_back(bone_a_next->next_joint.y);
		points.push_back(bone_a_next->next_joint.z);
		points.push_back(bone_b_next->next_joint.x);
		points.push_back(bone_b_next->next_joint.y);
		points.push_back(bone_b_next->next_joint.z);

		// std::vector<GLfloat> sampled_points_b = this->BarycentricSampling(points, 10);
		std::vector<GLfloat> sampled_points_b = this->PoissonSampling(points);
		frame_points.insert(frame_points.end(), sampled_points_b.begin(), sampled_points_b.end());
		points.clear();
	}
}

void FrameSampler::ParsePalm(const LEAP_HAND* hand)
{
	const LEAP_BONE* thumb_bone = &hand->digits[0].bones[0];
	const LEAP_BONE* index_bone = &hand->digits[1].bones[0];
	const LEAP_BONE* middle_bone = &hand->digits[2].bones[0];
	const LEAP_BONE* ring_bone = &hand->digits[3].bones[0];
	const LEAP_BONE* pinky_bone = &hand->digits[4].bones[0];
	std::vector<GLfloat> points;

	points.push_back(index_bone->next_joint.x);
	points.push_back(index_bone->next_joint.y);
	points.push_back(index_bone->next_joint.z);
	points.push_back(middle_bone->next_joint.x);
	points.push_back(middle_bone->next_joint.y);
	points.push_back(middle_bone->next_joint.z);
	points.push_back(index_bone->prev_joint.x);
	points.push_back(index_bone->prev_joint.y);
	points.push_back(index_bone->prev_joint.z);

	std::vector<GLfloat> sampled_points = this->PoissonSampling(points);
	frame_points.insert(frame_points.end(), sampled_points.begin(), sampled_points.end());
	sampled_points.clear();
	points.clear();

	points.push_back(middle_bone->next_joint.x);
	points.push_back(middle_bone->next_joint.y);
	points.push_back(middle_bone->next_joint.z);
	points.push_back(ring_bone->next_joint.x);
	points.push_back(ring_bone->next_joint.y);
	points.push_back(ring_bone->next_joint.z);
	points.push_back(index_bone->prev_joint.x);
	points.push_back(index_bone->prev_joint.y);
	points.push_back(index_bone->prev_joint.z);

	sampled_points = this->PoissonSampling(points);
	frame_points.insert(frame_points.end(), sampled_points.begin(), sampled_points.end());
	sampled_points.clear();
	points.clear();

	points.push_back(ring_bone->next_joint.x);
	points.push_back(ring_bone->next_joint.y);
	points.push_back(ring_bone->next_joint.z);
	points.push_back(pinky_bone->next_joint.x);
	points.push_back(pinky_bone->next_joint.y);
	points.push_back(pinky_bone->next_joint.z);
	points.push_back(index_bone->prev_joint.x);
	points.push_back(index_bone->prev_joint.y);
	points.push_back(index_bone->prev_joint.z);

	sampled_points = this->PoissonSampling(points);
	frame_points.insert(frame_points.end(), sampled_points.begin(), sampled_points.end());
	sampled_points.clear();
	points.clear();

	points.push_back(pinky_bone->next_joint.x);
	points.push_back(pinky_bone->next_joint.y);
	points.push_back(pinky_bone->next_joint.z);
	points.push_back(pinky_bone->prev_joint.x);
	points.push_back(pinky_bone->prev_joint.y);
	points.push_back(pinky_bone->prev_joint.z);
	points.push_back(index_bone->prev_joint.x);
	points.push_back(index_bone->prev_joint.y);
	points.push_back(index_bone->prev_joint.z);
	
	sampled_points = this->PoissonSampling(points);
	frame_points.insert(frame_points.end(), sampled_points.begin(), sampled_points.end());
	sampled_points.clear();
	points.clear();
}

void FrameSampler::SampleFrame(const LEAP_TRACKING_EVENT* frame)
{
	if (frame->nHands > 0) {
		for (int i = 0; i < frame->nHands; ++i) {
			const LEAP_HAND* hand = &frame->pHands[i];
			for (int f = 0; f < 4; ++f) {
				this->ParseFingers(&hand->digits[f], &hand->digits[f + 1]);
			}

			this->ParsePalm(hand);
		}
	}
}

std::vector<GLfloat> FrameSampler::BarycentricSampling(std::vector<GLfloat> points, int count)
{
	srand(955631);
	int vertices = points.size();
	std::vector<float> r1, r2;
	for (int i = 0; i < count; ++i) {
		r1.push_back(static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
		r2.push_back(static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
	}
	for (int i = 0; i < count; ++i) {
		float coeff_1 = 1 - std::sqrt(r1[i]);
		float coeff_2 = (std::sqrt(r1[i])) * (1 - r2[i]);
		float coeff_3 = std::sqrt(r1[i]) * r2[i];
		points.push_back(coeff_1*points[0] + coeff_2 * points[3] + coeff_3 * points[6]);
		points.push_back(coeff_1*points[1] + coeff_2 * points[4] + coeff_3 * points[7]);
		points.push_back(coeff_1*points[2] + coeff_2 * points[5] + coeff_3 * points[8]);
	}

	return points;
}

/*
std::vector<GLfloat> FrameSampler::StratifiedSampling(std::vector<GLfloat> points, int count)
{

}
*/

std::vector<GLfloat> FrameSampler::PoissonSampling(std::vector<GLfloat> points, float density)
{
	float area = TriangleArea(points);
	int num_points = area * density;
	return this->BarycentricSampling(points, num_points);
}

FrameSampler::~FrameSampler() { }