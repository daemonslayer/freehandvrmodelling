/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "DirectionalLight.h"

DirectionalLight::DirectionalLight() { }

void DirectionalLight::Init()
{
	this->model_matrix = glm::mat4(1.0f);
	this->model_matrix = glm::translate(this->model_matrix, glm::vec3(0.0f, -300.0f, -500.0f));
	this->model_matrix = glm::rotate(this->model_matrix, glm::radians(70.0f), glm::vec3(1.0f));

	glUseProgram(*(this->program));
	glBindVertexArray(*vao);
	program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
	glUseProgram(0);
}

void DirectionalLight::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());
	
}

void DirectionalLight::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void DirectionalLight::Clean()
{
	this->vao = nullptr;
	this->vbo = nullptr;
	this->program = nullptr;
}

DirectionalLight::~DirectionalLight() { this->Clean(); }