/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __BUCKET_H__
#define __BUCKET_H__
#pragma once

#include <vector>
#include <chrono>
#include <GL/glew.h>
#include "Timestamp.h"

#define MAX_FRAMES 200

enum class BUCKETSTATUS {
    SUCCESS,
    BUCKET_FULL,
    NULL_BUCKET,
};

class Bucket {
private:
    static int size_of_vector;
    static bool sizeInitialized;
    static int total_buckets;
    int bucket_id;
    int num_frames;
	Timestamp timestamp;
    std::vector<float> points_buffer;
    GLuint vbo_bucket;

public:
    Bucket();
    Bucket(int size_of_vector);
    virtual ~Bucket();

	void SetSizeOfPoints(int size);
    BUCKETSTATUS AddPointsToBucket(std::vector<float> points);
    bool IfSpacePresent();
    int GetBucketID();
    GLuint SetupVertexBuffer();
    int GetSizeOfVector();
};

#endif  // __BUCKET_H__