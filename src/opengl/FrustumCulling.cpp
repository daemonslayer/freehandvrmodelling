/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  CopyRIGHT_PLANE (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "FrustumCulling.h"

FrustumCulling::FrustumCulling() { }

void FrustumCulling::CalculateFrustumPlanes(Camera& camera)
{
	// TODO: fix frustum culling
	// if (camera.camera_variables_changed) {
	// 	camera.camera_variables_changed = false;
		this->model_matrix = glm::mat4(1.0f);
		this->view_matrix = camera.GetViewMatrix();
		// this->projection_matrix = glm::persepective();
		this->clipping_plane = this->projection_matrix * this->view_matrix * this->model_matrix;
		planes[TOP_PLANE][0] = clipping_plane[0][3] - clipping_plane[0][1];
		planes[TOP_PLANE][1] = clipping_plane[1][3] - clipping_plane[1][1];
		planes[TOP_PLANE][2] = clipping_plane[2][3] - clipping_plane[2][1];
		planes[TOP_PLANE][3] = clipping_plane[3][3] - clipping_plane[3][1];

		planes[BOTTOM_PLANE][0] = clipping_plane[0][3] + clipping_plane[0][1];
		planes[BOTTOM_PLANE][1] = clipping_plane[1][3] + clipping_plane[1][1];
		planes[BOTTOM_PLANE][2] = clipping_plane[2][3] + clipping_plane[2][1];
		planes[BOTTOM_PLANE][3] = clipping_plane[3][3] + clipping_plane[3][1];

		planes[LEFT_PLANE][0] = clipping_plane[0][3] + clipping_plane[0][0];
		planes[LEFT_PLANE][1] = clipping_plane[1][3] + clipping_plane[1][0];
		planes[LEFT_PLANE][2] = clipping_plane[2][3] + clipping_plane[2][0];
		planes[LEFT_PLANE][3] = clipping_plane[3][3] + clipping_plane[3][0];

		planes[RIGHT_PLANE][0] = clipping_plane[0][3] - clipping_plane[0][0];
		planes[RIGHT_PLANE][1] = clipping_plane[1][3] - clipping_plane[1][0];
		planes[RIGHT_PLANE][2] = clipping_plane[2][3] - clipping_plane[2][0];
		planes[RIGHT_PLANE][3] = clipping_plane[3][3] - clipping_plane[3][0];

		planes[NEAR_PLANE][0] = clipping_plane[0][3] - clipping_plane[0][2];
		planes[NEAR_PLANE][1] = clipping_plane[1][3] - clipping_plane[1][2];
		planes[NEAR_PLANE][2] = clipping_plane[2][3] - clipping_plane[2][2];
		planes[NEAR_PLANE][3] = clipping_plane[3][3] - clipping_plane[3][2];

		planes[FAR_PLANE][0] = clipping_plane[0][3] + clipping_plane[0][2];
		planes[FAR_PLANE][1] = clipping_plane[1][3] + clipping_plane[1][2];
		planes[FAR_PLANE][2] = clipping_plane[2][3] + clipping_plane[2][2];
		planes[FAR_PLANE][3] = clipping_plane[3][3] + clipping_plane[3][2];

		planes[TOP_PLANE] = glm::normalize(planes[TOP_PLANE]);
		planes[BOTTOM_PLANE] = glm::normalize(planes[BOTTOM_PLANE]);
		planes[LEFT_PLANE] = glm::normalize(planes[LEFT_PLANE]);
		planes[RIGHT_PLANE] = glm::normalize(planes[RIGHT_PLANE]);
		planes[NEAR_PLANE] = glm::normalize(planes[NEAR_PLANE]);
		planes[FAR_PLANE] = glm::normalize(planes[FAR_PLANE]);
	// }
}

bool FrustumCulling::ContainsPoint(glm::vec3& point)
{
	for (int i = 0; i < 6; ++i) {
		if (planes[i][0] * point[0] + planes[i][1] * point[1] + planes[i][2] * point[2] + planes[i][3] <= 0) {
			return false;
		}
	}
	return true;
}

bool FrustumCulling::ContainsBoundingSphere(BoundingSphere& bounding_sphere)
{
	float distance = 0.0f;
	float radius = bounding_sphere.GetRadius();
	glm::vec3 center = bounding_sphere.GetCenter();
	for (int i = 0; i < 6; ++i) {
		distance = planes[i][0] * center[0] + planes[i][1] * center[1] + planes[i][2] * center[2] + planes[i][3] <= 0;
		if (distance < radius)
			return false;
	}
	return true;
}

bool FrustumCulling::ContainsBoundingCube(BoundingBox& aligned_cube)
{
	return false;
}

FrustumCulling::~FrustumCulling() { }
