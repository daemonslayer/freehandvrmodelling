/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "GestureActor.h"

GestureActor::GestureActor() { }
void GestureActor::SetActorForGesture(GESTURE_TYPE type)
{
	switch (type) {
	case GESTURE_TYPE::HOLD_GESTURE:
	{
		std::cout << "Hand Hold Gesture recorded" << std::endl;
		break;
	}

	case GESTURE_TYPE::FINGERPOINT_GESTURE:
	{
		// pointer.Render(camera);
	}

	case GESTURE_TYPE::FACINGHANDS_GESTURE:
	{
		/*
		// glm::vec3 axis = info.GetAxis();
		glm::vec3 axis = glm::vec3(1.0f);
		glm::vec3 pivot = avg_frame.GetPivot();
		float angle = avg_frame.GetAngle();
		// float scale = avg_frame.GetDistanceBetweenHands();
		glm::vec3 zoom = avg_frame.GetZoom();

		cube.Update(angle * 10, pivot, axis);
		// scale down by 50: translation too much otherwise
		// camera.OffsetPosition(glm::vec3(0.0f, 0.0f, std::max(std::max(zoom.x, zoom.y), zoom.z) / 50));
		glm::mat4 projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
			(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());
		cube.Update(camera.GetViewMatrix(), projection_matrix);
		*/
	}

	default:
		// std::cout << "No Gesture Recorded" << std::endl;
		break;
	}
}