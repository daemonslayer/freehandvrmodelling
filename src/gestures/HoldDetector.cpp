/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "HoldDetector.h"
#include <iostream>
#include <vector>

#define HOLD_THRESHOLD 220.0 // Sum of Distances between PalmCenter and Fingers for a fist

HoldGestureDetector::HoldGestureDetector() : 
	gesture_type(GESTURE_TYPE::HOLD_GESTURE),
	hand_type(HAND_TYPE::BOTH_HANDS),
	hold_start_threshold(HOLD_THRESHOLD) { }
HoldGestureDetector::HoldGestureDetector(HAND_TYPE hand_type) : 
	gesture_type(GESTURE_TYPE::HOLD_GESTURE), 
	hand_type(hand_type),
	hold_start_threshold(HOLD_THRESHOLD) { }

double HoldGestureDetector::SquaredDistance(LEAP_VECTOR a, LEAP_VECTOR b) {
	return std::pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2);
}

void HoldGestureDetector::CheckLeapFrame(const LEAP_TRACKING_EVENT* frame)
{
	if (this->hand_type == HAND_TYPE::NONE) {
		std::cout << "[-] Hold Gesture requires atleast one hand" << std::endl;
		exit(0);
	}
	const LEAP_HAND* hand = &frame->pHands[0];
	LEAP_VECTOR palm_position = hand->palm.position;
	std::vector<double> fingers_distance_from_palm;

	for (int f = 0; f < 5; ++f) {
		const LEAP_DIGIT finger = hand->digits[f];
		for (int b = 0; b < 4; ++b) {
			const LEAP_BONE bone = finger.bones[b];
			fingers_distance_from_palm.push_back(this->SquaredDistance(bone.next_joint, palm_position));
		}
	}

	double sum_distances = 0;
	for (int i = 0; i < fingers_distance_from_palm.size(); ++i) {
		sum_distances += fingers_distance_from_palm[i];
	}
	sum_distances = std::sqrt(sum_distances);
	if (sum_distances <= this->hold_start_threshold) {
		this->is_gesture_active = true;
	}
	else this->is_gesture_active = false;
}

GESTURE_TYPE HoldGestureDetector::Type() { return this->gesture_type; }
void HoldGestureDetector::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

bool HoldGestureDetector::IsGestureActive(const LEAP_TRACKING_EVENT* frame)
{
	if (frame->nHands <= 0) return false;
	this->CheckLeapFrame(frame);
	if (this->is_gesture_active) {
		return true;
	}
	else {
		return false;
	}
}

void HoldGestureDetector::RegisterGestureListener(IGestureListener* listener)
{

}

void HoldGestureDetector::RemoveGestureListener(IGestureListener* listener)
{

}

void HoldGestureDetector::NotifyGestureListener(bool is_active)
{

}