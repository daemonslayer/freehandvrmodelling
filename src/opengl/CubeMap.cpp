#include "CubeMap.h"
#ifndef STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_IMPLEMENTATION
#endif // stbi
#include <iostream>


CubeMap::CubeMap() { glGenTextures(1, &this->texture_programme); }
CubeMap::~CubeMap() { }

void CubeMap::LoadCubeMap(std::vector<std::string>& faces, int internalFormat)
{
	glBindTexture(GL_TEXTURE_2D, this->texture_programme);

	this->image_width = this->image_height = this->image_channels = 0;
	for (unsigned int i = 0; i < faces.size(); i++) {
		stbi_set_flip_vertically_on_load(true);
		unsigned char *data = stbi_load(faces[i].c_str(), &this->image_width, &this->image_height, &this->image_channels, 0);
		if (data) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, this->image_width, this->image_height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
		else {
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

CubeMap::operator GLuint() const { return this->texture_programme; }
