/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __PROGRAM_H__
#define __PROGRAM_H__
#pragma once

#include <string>
#include <exception>
#include <glm/glm.hpp>
#include "Platform.h"
#include "Shader.h"

class LinkException : public std::exception
{
public:
	LinkException(const std::string& str) throw() : infoLog(str) {}
	virtual ~LinkException() throw() {}

	virtual const char* what() const throw()
	{
		return infoLog.c_str();
	}

private:
	std::string infoLog;
};

class Program
{
	typedef unsigned char uchar;
	typedef unsigned short ushort;
	typedef unsigned int uint;
	typedef unsigned long ulong;
	typedef GLint Attribute;
	typedef GLint Uniform;
	typedef GLint UniformSampler2d;

private:
	GLuint shader_programme;

public:
	Program();
	Program(const Shader& vertex);
	Program(const Shader& vertex, const Shader& fragment);
	Program(const Shader& vertex, const Shader& fragment, const Shader& geometry);
	virtual ~Program() = default;

	operator GLuint() const;
	const Program& operator=(const Program& other);

	void Attach(const Shader& shader);
	void TransformFeedbackVaryings(const char** varyings, uint count);
	void Link();

	std::string GetInfoLog();

	Attribute GetAttribute(const std::string& name);
	void SetAttribute(const std::string& name, GLuint index);
	Uniform GetUniform(const std::string& name);

	void SetUniform(const Uniform& uniform, int value);
	void SetUniform(const Uniform& uniform, float value);
	void SetUniform(const Uniform& uniform, const glm::vec2& value);
	void SetUniform(const Uniform& uniform, const glm::vec3& value);
	void SetUniform(const Uniform& uniform, const glm::vec4& value);
	void SetUniform(const Uniform& uniform, const float* values, uint count);
	void SetUniform(const Uniform& uniform, const glm::vec2* values, uint count);
	void SetUniform(const Uniform& uniform, const glm::vec3* values, uint count);
	void SetUniform(const Uniform& uniform, const glm::vec4* values, uint count);
	void SetUniform(const Uniform& uniform, const glm::mat3& value);
	void SetUniform(const Uniform& uniform, const glm::mat4& value);
	void SetUniformSampler2d(const UniformSampler2d& uniform_sampler2d, int location);
};

#endif // __PROGRAM_H__