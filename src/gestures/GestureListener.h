/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#pragma once
#include <vector>
#include "LeapConnection.h"
#include "Gesture.h"
#include "IGestureDetector.h"
#include "IGestureListener.h"
#include "HoldDetector.h"
#include "ExtendedFingerDetector.h"
#include "PinchDetector.h"
#include "SwipeDetector.h"
#include "FingerPointDetector.h"
#include "PinchDrawDetector.h"
#include "DoubleTwoHandHoldDetector.h"
#include "FacingHandsDetector.h"

class GestureListener
{
private:
	std::vector<IGestureDetector*> gesture_detectors;
	std::vector<IGestureListener*> gesture_listeners;
	GESTURE_TYPE last_active_gesture;

public:
	GestureListener();
	virtual ~GestureListener() = default;

	bool EnableGesture(GESTURE_TYPE gesture_type);
	bool DisableGesture(GESTURE_TYPE gesture_type);
	bool DisableAllGestures();
	GESTURE_TYPE CheckFrameForGesture();
	GESTURE_TYPE GestureActive(GESTURE_TYPE gesture_type);

};

