/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "FingerPointDetector.h"
#include <iostream>



FingerPointDetector::FingerPointDetector() :
	gesture_type(GESTURE_TYPE::FINGERPOINT_GESTURE) { }

void FingerPointDetector::CheckLeapFrame(const LEAP_TRACKING_EVENT* frame)
{
	if (frame->nHands > 1) {
		return;
	}
	const LEAP_HAND* hand = &frame->pHands[0];
	
	if (hand->index.is_extended &&
		!hand->thumb.is_extended &&
		!hand->middle.is_extended &&
		!hand->ring.is_extended &&
		!hand->pinky.is_extended) {
		this->is_gesture_active = true;
	}
	else {
		this->is_gesture_active = false;
	}
}

GESTURE_TYPE FingerPointDetector::Type() { return this->gesture_type; }
void FingerPointDetector::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

bool FingerPointDetector::IsGestureActive(const LEAP_TRACKING_EVENT* frame) 
{
	if (frame->nHands <= 0) return false;
	this->CheckLeapFrame(frame);
	if (this->is_gesture_active) {
		return true;
	}
	else {
		return false;
	}
}

void FingerPointDetector::RegisterGestureListener(IGestureListener* listener) 
{

}

void FingerPointDetector::RemoveGestureListener(IGestureListener* listener) 
{

}

void FingerPointDetector::NotifyGestureListener(bool is_active) 
{

}