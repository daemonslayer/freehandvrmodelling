/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "Camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <SDL.h>
#include <iostream>

Camera::Camera(glm::vec3 position, glm::vec3 direction, glm::vec3 up) :
	fov(FOV),
	near_plane(NEAR_PLANE),
	far_plane(FAR_PLANE),
	viewport_aspect_ratio(WINDOW_WIDTH/WINDOW_HEIGHT),
	camera_speed(CAMERA_SPEED)
{
	this->position = position;
	this->direction = direction;
	this->up_vector = up;
}

Camera::Camera(float posX, float posY, float posZ, float directionX, float directionY, float directionZ, float upX, float upY, float upZ) :
	fov(FOV),
	near_plane(FAR_PLANE),
	far_plane(FAR_PLANE),
	viewport_aspect_ratio(WINDOW_WIDTH / WINDOW_HEIGHT),
	camera_speed(CAMERA_SPEED)
{
	this->position = glm::vec3(posX, posY, posZ);
	this->up_vector = glm::vec3(upX, upY, upZ);
}

glm::vec3 Camera::GetPosition() const { return this->position; }
void Camera::SetPosition(const glm::vec3& position) {}
void Camera::OffsetPosition(const glm::vec3& offset_value) {}

glm::vec3 Camera::GetDirection() const { return this->direction; }
void Camera::SetDirection(const glm::vec3& direction) { this->direction = direction; }

float Camera::GetFieldOfView() const { return this->fov; }
void Camera::SetFieldOfView(float fov) { this->fov = fov; }

float Camera::GetNearPlane() const { return this->near_plane; }
void Camera::SetNearPlane(float near_plane) {}
float Camera::GetFarPlane() const { return this->far_plane; }
void Camera::SetFarPlane(float far_plane) {}
void Camera::SetNearFarPlanes(float near_plane, float far_plane) {}

float Camera::GetViewportAspectRatio() const { return this->viewport_aspect_ratio; }
void Camera::SetViewportAspectRatio(float viewport_aspect_ratio) {}

float Camera::GetCameraSpeed() const { return this->camera_speed; }
void Camera::SetCameraSpeed(float speed) { this->camera_speed = speed; }

// TODO: Fix camera motion and add mouse control
void Camera::UpdateLookAt()
{
	const Uint8* keystate = SDL_GetKeyboardState(NULL);
	if (keystate[SDL_SCANCODE_W] || keystate[SDL_SCANCODE_UP]) {
		glm::vec3 camera_position = glm::vec3(0.0f, -300.0f, -500.0f);
		glm::vec3 direction = glm::normalize(camera_position - (camera_position + glm::vec3(0.0f, 0.0f, -1.0f)));
		this->position += camera_speed * direction;
		this->direction += camera_speed * direction;
	}

	if (keystate[SDL_SCANCODE_S] || keystate[SDL_SCANCODE_DOWN]) {
		glm::vec3 direction = glm::vec3(0.0f, 0.0f, -1.0f);
		this->position += camera_speed * direction;
		this->direction += camera_speed * direction;
	}

	if (keystate[SDL_SCANCODE_D] || keystate[SDL_SCANCODE_RIGHT]) {
		glm::vec3 direction = glm::vec3(-1.0f, 0.0f, 0.0f);
		this->position += camera_speed * direction;
		this->direction += camera_speed * direction;
	}

	if (keystate[SDL_SCANCODE_A] || keystate[SDL_SCANCODE_LEFT]) {
		glm::vec3 direction = glm::vec3(1.0f, 0.0f, 0.0f);
		this->position += camera_speed * direction;
		this->direction += camera_speed * direction;
	}
}

glm::mat4 Camera::GetViewMatrix() const { return glm::lookAt(this->position, this->direction, this->up_vector); }
