#ifndef __INTERFACE_H__
#define __INTERFACE_H__
#pragma once

#include <vector>
#include "Gesture.h"

enum class MODE
{
	DRAW,
	EDIT,
	VIEW,
	USERINTERFACE,
	NONE,
};

enum class ACTIONS
{
	START,
	STOP,
	ERASE,
	ERASEALL,
	DEFORM,
	ROI_BEAUTIFICATION,
	ROTATE,
	SCALE,
	TRANSLATE,
	TELEPORT,
	NONE,
};

extern const std::vector<GESTURE_TYPE> QuickAccessGestures;
extern const std::vector<GESTURE_TYPE> DrawModeGestures;
extern const std::vector<GESTURE_TYPE> EditModeGestures;
extern const std::vector<GESTURE_TYPE> ViewModeGestures;
extern const std::vector<GESTURE_TYPE> UserInterfaceModeGestures;
extern const std::vector<ACTIONS> DrawModeActions;
extern const std::vector<ACTIONS> EditModeActions;
extern const std::vector<ACTIONS> ViewModeActions;
extern const std::vector<ACTIONS> UserInterfaceModeActions;

class Interface {
public:
	virtual MODE CurrentMode() = 0;
	virtual const std::vector<ACTIONS> PossibleActionsInCurrentMode() = 0;
};

#endif