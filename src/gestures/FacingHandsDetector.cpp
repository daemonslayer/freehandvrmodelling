/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "FacingHandsDetector.h"
#include <glm/glm.hpp>
#include <iostream>
#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>

#define ANGLE_THRESHOLD 10

FacingHandsDetector::FacingHandsDetector() :
	gesture_type(GESTURE_TYPE::FACINGHANDS_GESTURE),
	angle_threshold(ANGLE_THRESHOLD) {
	this->extendedfingerDetector = new ExtendedFingerDetector();
	extendedfingerDetector->ActiveHand(HAND_TYPE::BOTH_HANDS);
}

float FacingHandsDetector::AngleBetweenPalmDirections(LEAP_VECTOR* PalmDirections) {
	float dot_product = PalmDirections[0].x*PalmDirections[1].x +
		PalmDirections[0].y*PalmDirections[1].y +
		PalmDirections[0].z*PalmDirections[1].z;
	float squared_length_a = PalmDirections[0].x*PalmDirections[0].x +
		PalmDirections[0].y*PalmDirections[0].y +
		PalmDirections[0].z*PalmDirections[0].z;
	float squared_length_b = PalmDirections[1].x*PalmDirections[1].x +
		PalmDirections[1].y*PalmDirections[1].y +
		PalmDirections[1].z*PalmDirections[1].z;
	return std::acos(dot_product / std::sqrt(squared_length_a * squared_length_b)) * 180.0 / M_PI;
}

void FacingHandsDetector::CheckLeapFrame(const LEAP_TRACKING_EVENT* frame)
{
	LEAP_VECTOR PalmDirection[2];
	if (this->extendedfingerDetector->IsGestureActive(frame) && frame->nHands == 2) {
		for (int i = 0; i < frame->nHands; ++i) {
			const LEAP_HAND* hand = &frame->pHands[i];
			PalmDirection[i] = hand->palm.direction;
		}
		if (AngleBetweenPalmDirections(PalmDirection) > this->angle_threshold) {
			this->is_gesture_active = true;
		}

		else {
			this->is_gesture_active = false;
		}
	}
	else {
		this->is_gesture_active = false;
	}
}

GESTURE_TYPE FacingHandsDetector::Type() { return this->gesture_type; }
void FacingHandsDetector::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

bool FacingHandsDetector::IsGestureActive(const LEAP_TRACKING_EVENT* frame)
{
	this->CheckLeapFrame(frame);
	if (this->is_gesture_active) {
		return true;
	}
	else {
		return false;
	}
}

void FacingHandsDetector::RegisterGestureListener(IGestureListener* listener)
{

}

void FacingHandsDetector::RemoveGestureListener(IGestureListener* listener)
{

}

void FacingHandsDetector::NotifyGestureListener(bool is_active)
{

}
