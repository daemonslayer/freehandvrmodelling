#pragma once
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "LeapC.h"
#include "LeapConnection.h"

enum class Sampling {
	BARYCENTRIC,
	STRATIFIED,
	POISSON,
};

#define POINTS_DENSITY 0.1

class FrameSampler {
private:

	// std::vector<GLfloat> StratifiedSampling(std::vector<GLfloat> points, int count = 10);
	std::vector<GLfloat> BarycentricSampling(std::vector<GLfloat> points, int count = 10);
	std::vector<GLfloat> PoissonSampling(std::vector<GLfloat> points, float density = POINTS_DENSITY);
	
	std::vector<GLfloat> frame_points;
	void ParseFingers(const LEAP_DIGIT* finger_a, const LEAP_DIGIT* finger_b);
	void ParsePalm(const LEAP_HAND* hand);
	float TriangleArea(std::vector<GLfloat> points);

public:
	FrameSampler();
	virtual ~FrameSampler();

	std::vector<GLfloat> SampleCurrentFrame();
	void SampleFrame(const LEAP_TRACKING_EVENT* frame);
	
};