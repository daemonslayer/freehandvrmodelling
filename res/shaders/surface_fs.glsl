#version 450

in FragmentData 
{
  vec2 texcoord;
  vec3 normal;
  vec3 position;
} FragmentIn;
out vec4 fragmentColor;

uniform vec3 lightPosition;

void main()
{
  if ((pow(FragmentIn.texcoord.x, 2.0) + pow( FragmentIn.texcoord.y, 2.0) > 1.0))
    discard;
  vec3 light_vec = normalize(lightPosition - FragmentIn.position);
  vec3 color = max(0.0, dot(FragmentIn.normal, -light_vec)) * vec3(1.0, 0.0, 0.0);
  fragmentColor = vec4(color, 1.0);
}
