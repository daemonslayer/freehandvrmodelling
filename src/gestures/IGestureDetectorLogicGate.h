/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#pragma once

#include <vector>
#include <chrono>
#include "Gesture.h"
#include "IGestureDetector.h"
#include "PinchDetector.h"
#include "SwipeDetector.h"
#include "FingerPointDetector.h"
#include "HoldDetector.h"
#include "ExtendedFingerDetector.h"

class IGestureDetectorLogicGate :
	public IGestureDetector
{
protected:
	std::vector<IGestureDetector*> gesture_detectors;
	std::vector<LOGICGATE_TYPE> detector_logic_gates;
	std::vector<bool> detector_negate;
	std::vector<std::chrono::seconds> valid_window_for_gesture;
	std::vector<bool> detector_activated;
	std::chrono::steady_clock::time_point time_of_last_gesture_detection;

	bool CheckState(const LEAP_TRACKING_EVENT* frame);
	void CheckLeapFrame(const LEAP_TRACKING_EVENT* frame);
	HAND_TYPE hand_type;

	bool is_gesture_active;
	int last_update_frame;
	int last_update_gesture;
	bool did_change;
	bool is_gesture_started;
	bool is_gesture_complete;
	bool current_state;

public:
	IGestureDetectorLogicGate();
	virtual ~IGestureDetectorLogicGate() = default;

	bool IsGestureActive(const LEAP_TRACKING_EVENT* frame) override;
	void ActiveHand(HAND_TYPE hand_type) override;
	void AddGesture(GESTURE_TYPE gesture_type,
		LOGICGATE_TYPE gesture_logicgate = LOGICGATE_TYPE::AND_GATE,
		bool detector_negate = false,
		std::chrono::seconds gesture_window = std::chrono::seconds(0),
		HAND_TYPE hand_type = HAND_TYPE::RIGHT_HAND);
	void RemoveGesture(GESTURE_TYPE gesture_type);

	void InitCompositeGesture();
	void ResetCompositeGesture();
};

