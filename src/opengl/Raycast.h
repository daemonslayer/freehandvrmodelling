#pragma once
#include <vector>
#include "Object.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "Program.h"


class Raycast
{
private:
	glm::vec3 origin;
	glm::vec3 direction;

public:
	Raycast();
	virtual ~Raycast() = default;

	glm::vec3 GetOrigin();
	void SetOrigin(glm::vec3 origin);
	glm::vec3 GetDirection();
	void SetDirection(glm::vec3 direction);

};

