#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec3 fposition;
out vec3 fnormal;

void main()
{
	fnormal = normal;
	fposition = position;
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
}