#include "Interface.h"

const std::vector<GESTURE_TYPE> QuickAccessGestures = { GESTURE_TYPE::DOUBLETWOHANDHOLD_GESTURE };
const std::vector<GESTURE_TYPE> DrawModeGestures = { GESTURE_TYPE::HOLD_GESTURE };
const std::vector<GESTURE_TYPE> EditModeGestures = { GESTURE_TYPE::HOLD_GESTURE };
const std::vector<GESTURE_TYPE> ViewModeGestures = { GESTURE_TYPE::HOLD_GESTURE };
const std::vector<GESTURE_TYPE> UserInterfaceModeGestures = { GESTURE_TYPE::HOLD_GESTURE };
const std::vector<ACTIONS> DrawModeActions = { ACTIONS::START, ACTIONS::STOP };
const std::vector<ACTIONS> EditModeActions = { ACTIONS::ERASE,
	ACTIONS::ERASEALL, ACTIONS::DEFORM, ACTIONS::ROI_BEAUTIFICATION };
const std::vector<ACTIONS> ViewModeActions = { ACTIONS::ROTATE,
	ACTIONS::SCALE, ACTIONS::TRANSLATE, ACTIONS::TELEPORT };
const std::vector<ACTIONS> UserInterfaceModeActions = { ACTIONS::NONE };