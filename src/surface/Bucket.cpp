/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include <iostream>
#include "Bucket.h"

int Bucket::total_buckets = 0;
int Bucket::size_of_vector = -1;
bool Bucket::sizeInitialized = false;
void Bucket::SetSizeOfPoints(int size) {
    if (!sizeInitialized)
        Bucket::size_of_vector = size;
    else
        std::cout << "[-] Cannot change size of points once initialized" << std::endl;
}

Bucket::Bucket() {
    this->num_frames = 0;
    this->timestamp.StoreThisTime();
    Bucket::total_buckets += 1; 
}

Bucket::Bucket(int size_of_vector) {
    this->num_frames = 0;
    this->timestamp.StoreThisTime();
    Bucket::total_buckets += 1; 
    this->size_of_vector = size_of_vector;
}

BUCKETSTATUS Bucket::AddPointsToBucket(std::vector<float> points) {
    if (this->num_frames >= MAX_FRAMES) {
        return BUCKETSTATUS::BUCKET_FULL;
    } 
    else {
        this->points_buffer.insert(this->points_buffer.end(), points.begin(), points.end());
        this->num_frames += 1;
        return BUCKETSTATUS::SUCCESS;
    }
}

bool Bucket::IfSpacePresent() { return this->num_frames >= MAX_FRAMES; }
int Bucket::GetBucketID() { return this->bucket_id; }
int Bucket::GetSizeOfVector() { return this->size_of_vector; }

// TODO: Set size of buffered_data as a constant large value
GLuint Bucket::SetupVertexBuffer() {
    glGenBuffers(1, &this->vbo_bucket);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo_bucket);
    glBufferData(GL_ARRAY_BUFFER, this->points_buffer.size() * sizeof(GLfloat), &this->points_buffer[0], GL_DYNAMIC_DRAW);
    return this->vbo_bucket;
}

Bucket::~Bucket() {
    Bucket::total_buckets -= 1;
}