#include "Collision.h"


Collision::Collision() { }

RayHitInfo Collision::RayTriangleCollision(Raycast& ray, glm::vec3 points) const
{
	return RayHitInfo { 0 };
}

RayHitInfo Collision::RayObjectCollision(Raycast& ray, Object* object) const
{
	return RayHitInfo { 0 };
}

RayHitInfo Collision::RayCubeCollision(Raycast& ray, BoundingBox& box) const
{
	RayHitInfo result = { false, 0.0f, glm::vec3(0.0f), glm::vec3(0.0f) };
	float t[8];
	glm::vec3 cube_min = box.GetMinCorner();
	glm::vec3 cube_max = box.GetMaxCorner();
	glm::vec3 ray_origin = ray.GetOrigin();
	glm::vec3 ray_dir = ray.GetDirection();
	
	t[0] = (cube_min.x - ray_origin.x) / ray_dir.x;
	t[1] = (cube_max.x - ray_origin.x) / ray_dir.x;
	t[2] = (cube_min.y - ray_origin.y) / ray_dir.y;
	t[3] = (cube_max.y - ray_origin.y) / ray_dir.y;
	t[4] = (cube_min.z - ray_origin.z) / ray_dir.z;
	t[5] = (cube_max.z - ray_origin.z) / ray_dir.z;
	t[6] = (float)fmax(fmax(fmin(t[0], t[1]), fmin(t[2], t[3])), fmin(t[4], t[5]));
	t[7] = (float)fmin(fmin(fmax(t[0], t[1]), fmax(t[2], t[3])), fmax(t[4], t[5]));

	result.hit = !(t[7] < 0 || t[6] > t[7]);
	return result;
}

RayHitInfo Collision::RaySphereCollision(Raycast& ray, BoundingSphere& sphere) const
{

}

// collision between ray and Y-normal plane
RayHitInfo Collision::RayPlaneCollision(Raycast& ray, float plane_height) const
{
#define EPSILON 0.000001 
	glm::vec3 ray_origin = ray.GetOrigin();
	glm::vec3 ray_dir = ray.GetDirection();

	RayHitInfo result{ 0 };
	if (fabs(ray_dir.y > EPSILON)) {
		float distance = (ray_origin.y - plane_height / -ray_dir.y);
		if (distance >= 0.0f) {
			result.hit = true;
			result.distance = distance;
			result.normal = glm::vec3(0.0f, 1.0f, 0.0f);
			result.position = ray_origin + ray_dir * distance;
		}
	}

	return result;
}

RayHitInfo Collision::RayMeshCollision(const Raycast& ray, const Mesh& mesh)
{

}

Collision::~Collision() { }