#pragma once
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

class Logger
{
private:
	std::shared_ptr<spdlog::logger> logger;

public:
	Logger();
	virtual ~Logger();
};

