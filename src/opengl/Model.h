#pragma once
#include "Object.h"
#include <vector>
#include <string>
#include <map>
#include <filesystem>
#include <GL/glew.h>
#include <glm/gtx/quaternion.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>
#include "Mesh.h"
#include "MeshAnimation.h"
namespace fs = std::experimental::filesystem;

class Model : public Object
{
private:
	Assimp::Importer import;
	const aiScene* scene;
	std::vector<Mesh> meshes;
	std::string path;

	bool mesh_animation;
	aiMatrix4x4 m_global_inverse_transform;

	void ShowNodeName(aiNode* node);
	void ProcessNode(aiNode* node, const aiScene* scene);
	Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);
	GLuint LoadImageToTexture(std::string image_path);
	std::vector<MeshTexture> LoadMaterialTexture
		(aiMaterial* mat, aiTextureType type, std::string type_name);
	friend class MeshAnimation;

public:
	Model(bool mesh_animation = false);
	Model(std::string path, bool mesh_animation = false);
	virtual ~Model();
	static const unsigned int MAX_BONES = 100;
	
	void Init() override;
	void Render(Camera camera) override;
	void Update(glm::mat4 view_matrix, glm::mat4 projection_matrix) override;
	void Update(float angle, glm::vec3 pivot, glm::vec3 axis);
	void Clean() override;
	void LoadModel(std::string path);

};

