#version 450

in vec3 fposition;
in vec3 fnormal;

out vec4 fragmentColor;
uniform vec3 lightPosition;

void main()
{
	vec3 light_vec = normalize(lightPosition - fposition);
	vec3 normal = normalize(fnormal);
	vec3 color = abs(dot(normal, light_vec)) * vec3(1.0, 0.0, 0.0);
	fragmentColor = vec4(color, 1.0);
}