/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "DoubleTwoHandHoldDetector.h"



DoubleTwoHandHoldDetector::DoubleTwoHandHoldDetector() :
	gesture_type(GESTURE_TYPE::DOUBLETWOHANDHOLD_GESTURE) 
{
	this->InitCompositeGesture();
	this->AddGesture(GESTURE_TYPE::HOLD_GESTURE, LOGICGATE_TYPE::AND_GATE,
		false, std::chrono::seconds(0), HAND_TYPE::BOTH_HANDS);
	this->AddGesture(GESTURE_TYPE::EXTENDEDFINGER_GESTURE, LOGICGATE_TYPE::AND_GATE,
		false, std::chrono::seconds(5), HAND_TYPE::BOTH_HANDS);
	this->AddGesture(GESTURE_TYPE::HOLD_GESTURE, LOGICGATE_TYPE::AND_GATE,
		false, std::chrono::seconds(5), HAND_TYPE::BOTH_HANDS);
	this->AddGesture(GESTURE_TYPE::EXTENDEDFINGER_GESTURE, LOGICGATE_TYPE::AND_GATE,
		false, std::chrono::seconds(5), HAND_TYPE::BOTH_HANDS);
}

GESTURE_TYPE DoubleTwoHandHoldDetector::Type() { return this->gesture_type; }
void DoubleTwoHandHoldDetector::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

void DoubleTwoHandHoldDetector::RegisterGestureListener(IGestureListener* listener) 
{
	// this->
}

void DoubleTwoHandHoldDetector::RemoveGestureListener(IGestureListener* listener) 
{

}

void DoubleTwoHandHoldDetector::NotifyGestureListener(bool is_active) 
{

}