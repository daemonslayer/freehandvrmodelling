#include "BoundingSphere.h"

BoundingSphere::BoundingSphere(glm::vec3 center, float radius) :
	center(center),
	radius(radius) { }
void BoundingSphere::SetCenter(glm::vec3 center) { this->center = center; }
glm::vec3 BoundingSphere::GetCenter() { return this->center; }
void BoundingSphere::SetRadius(float radius) { this->radius = radius; }
float BoundingSphere::GetRadius() { return this->radius; }
BoundingSphere::~BoundingSphere() { }