#version 450

layout(location = 0) in vec3 position;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec3 fposition;

void main()
{
	fposition = position;
	gl_Position = projectionMatrix * viewMatrix * vec4(position, 1.0);
}