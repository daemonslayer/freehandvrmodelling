/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#ifndef __CONTEXT_H__
#define __CONTEXT_H__
#pragma once

#include <SDL.h>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include "Window.h"

class Window;
class Context
{
private:
	SDL_Window* gl_window;
	SDL_GLContext* gl_context;
	Context(SDL_Window* gl_window, SDL_GLContext* gl_context);
	friend class Window;

public:
	virtual ~Context();
	
	SDL_GLContext* GetWindowContext();
	SDL_Window* GetWindow();
	void GlewSetup();
	void EnableGL();
	void Clear(int buffer = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	void ClearColor(glm::vec4 Color = glm::vec4(0.45f, 0.55f, 0.60f, 1.00f));
};

#endif // __CONTEXT_H__