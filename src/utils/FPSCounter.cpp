#include "FPSCounter.h"



FPSCounter::FPSCounter() :
	start_time(0),
	pause_time(0), 
	paused(false),
	started(false) { }

void FPSCounter::Start()
{
	started = true;
	paused = false;
	start_time = SDL_GetTicks();
	pause_time = 0;
}

void FPSCounter::Stop()
{
	started = false;
	paused = false;
	start_time = 0;
	pause_time = 0;
}

void FPSCounter::Pause()
{
	if (started && !paused) {
		paused = true;
		pause_time = SDL_GetTicks() - start_time;
		start_time = 0;
	}
}

void FPSCounter::Play()
{
	if (started && paused) {
		paused = true;
		pause_time = 0;
		start_time = SDL_GetTicks() - pause_time;
	}
}

uint32_t FPSCounter::GetTicks()
{
	uint32_t time = 0;
	if (started) {
		if (paused) {
			time = pause_time;
		}
		else {
			time = SDL_GetTicks() - start_time;
		}
	}

	return time;
}

bool FPSCounter::IsStarted() { return started; }
bool FPSCounter::IsPaused() { return paused && started; }

FPSCounter::~FPSCounter() { }