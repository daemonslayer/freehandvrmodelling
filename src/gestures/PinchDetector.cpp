/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "PinchDetector.h"
#include <vector>
#include <iostream>

#define PINCH_THRESHOLD 15.0

PinchDetector::PinchDetector() : 
	gesture_type(GESTURE_TYPE::PINCH_GESTURE),
	pinch_start_threshold(PINCH_THRESHOLD) {}

GESTURE_TYPE PinchDetector::Type() { return this->gesture_type; }
void PinchDetector::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

void PinchDetector::CheckLeapFrame(const LEAP_TRACKING_EVENT* frame)
{
	std::vector<LEAP_VECTOR> thumb_joint_positions;
	// std::vector<Leap::Vector> thumb_joint_direction;
	std::vector<LEAP_VECTOR> index_joint_positions;
	// std::vector<Leap::Vector> index_joint_direction;

	const LEAP_HAND* hand = &frame->pHands[0];
	const LEAP_DIGIT thumb = hand->digits[0];
	for (int b = 0; b < 4; ++b) {
		const LEAP_BONE bone = thumb.bones[b];
		thumb_joint_positions.push_back(bone.next_joint);
	}
	// index
	const LEAP_DIGIT index_finger = hand->digits[1];
	for (int b = 0; b < 4; ++b) {
		const LEAP_BONE bone = index_finger.bones[b];
		index_joint_positions.push_back(bone.next_joint);
	}

	auto distance =
		std::sqrt(std::pow((thumb_joint_positions[3].x - index_joint_positions[3].x), 2) +
			std::pow((thumb_joint_positions[3].y - index_joint_positions[3].y), 2) +
			std::pow((thumb_joint_positions[3].z - index_joint_positions[3].z), 2));
	if (distance <= this->pinch_start_threshold) {
		this->is_gesture_active = true;
	}
	else {
		this->is_gesture_active = false;
	}
}

bool PinchDetector::IsGestureActive(const LEAP_TRACKING_EVENT* frame)
{
	if (frame->nHands <= 0) return false;
	this->CheckLeapFrame(frame);
	if (this->is_gesture_active) {
		std::cout << "Pinch gesture active" << std::endl;
		return true;
	}
	else {
		return false;
	}
}

void PinchDetector::RegisterGestureListener(IGestureListener* listener)
{

}

void PinchDetector::RemoveGestureListener(IGestureListener* listener)
{

}

void PinchDetector::NotifyGestureListener(bool is_active)
{

}