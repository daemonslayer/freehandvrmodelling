#include "GestureInterface.h"
#include <iostream>


GestureInterface::GestureInterface(MODE mode)
{ 
	gesture_listener.DisableAllGestures();
	this->current_mode = MODE::VIEW;
	std::vector<GESTURE_TYPE> gestures = this->GesturesInCurrentMode();
	for (int i = 0; i < gestures.size(); ++i) {
		gesture_listener.EnableGesture(gestures[i]);
	}
};

MODE GestureInterface::CurrentMode() { return this->current_mode; }

const std::vector<ACTIONS> GestureInterface::PossibleActionsInCurrentMode()
{
	if (this->current_mode == MODE::DRAW) {
		return DrawModeActions;
	}
	else if (this->current_mode == MODE::EDIT) {
		return EditModeActions;
	}
	else if (this->current_mode == MODE::USERINTERFACE) {
		return UserInterfaceModeActions;
	}
	else if (this->current_mode == MODE::VIEW) {
		return ViewModeActions;
	}
}

const std::vector<GESTURE_TYPE> GestureInterface::GesturesInCurrentMode()
{
	if (this->current_mode == MODE::DRAW) {
		return DrawModeGestures;
	}
	else if (this->current_mode == MODE::EDIT) {
		return EditModeGestures;
	}
	else if (this->current_mode == MODE::USERINTERFACE) {
		return UserInterfaceModeGestures;
	}
	else if (this->current_mode == MODE::VIEW) {
		return ViewModeGestures;
	}
}

bool GestureInterface::ChangeMode(MODE currentMode, MODE nextMode) 
{
	gesture_listener.DisableAllGestures();
	this->current_mode = nextMode;
	std::vector<GESTURE_TYPE> gestures = this->GesturesInCurrentMode();
	for (int i = 0; i < gestures.size(); ++i) {
		gesture_listener.EnableGesture(gestures[i]);
	}
	return true;
}

void GestureInterface::DrawModeRules()
{
	switch (this->gesture_listener.CheckFrameForGesture())
	{
		case GESTURE_TYPE::HOLD_GESTURE: 
		{
			std::cout << "Hands Detected" << std::endl;
		}
	}
}

void GestureInterface::EditModeRules()
{

}

void GestureInterface::UserInterfaceModeRules()
{

}

void GestureInterface::ViewModeRules()
{
	switch (this->gesture_listener.CheckFrameForGesture())
	{
		case GESTURE_TYPE::HOLD_GESTURE:
		{
			std::cout << "Hands Detected" << std::endl;
		}
	}
}

void GestureInterface::PollGestureDetectors()
{
	switch (this->current_mode) {
	case MODE::DRAW:
		this->DrawModeRules();
		break;
	case MODE::EDIT:
		this->DrawModeRules();
		break;
	case MODE::USERINTERFACE:
		this->UserInterfaceModeRules();
		break;
	case MODE::VIEW:
		this->ViewModeRules();
		break;
	}
}