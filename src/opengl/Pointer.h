#pragma once
#include <vector>
#include "Object.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "Program.h"


class Pointer : public Object
{
private:
	glm::mat4 model_matrix;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;

	std::vector<glm::vec3> ray_points;
	glm::vec3 direction;
	glm::vec3 origin;

	VertexArray* vao;
	VertexBuffer* vbo;
	Program* program;

public:
	Pointer();
	virtual ~Pointer();

	void Init() override;
	void Render(Camera camera) override;
	void Update(glm::mat4 view_matrix, glm::mat4 projection_matrix) override;
	void Update(glm::vec3 position);
	void Clean() override;
	glm::vec3 GetDirection();
	glm::vec3 GetOrigin();

};