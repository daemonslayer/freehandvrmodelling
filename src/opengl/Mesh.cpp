#include "Mesh.h"
#include <iostream>


Mesh::Mesh() { }
Mesh::Mesh(std::vector<MeshVertex> vertices, std::vector<unsigned int> indices, std::vector<MeshTexture> textures)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	this->SetupMesh();
	this->Init();
}

void Mesh::SetupMesh()
{
	this->vao = new VertexArray();
	this->vbo = new VertexBuffer(&this->vertices[0], this->vertices.size() * sizeof(MeshVertex), GL_DYNAMIC_DRAW);
	this->ebo = new ElementBuffer(&this->indices[0], this->indices.size() * sizeof(unsigned int), GL_DYNAMIC_DRAW);
	Shader m_vert(ShaderType::Vertex, "res/shaders/mesh_vs.glsl");
	Shader m_frag(ShaderType::Fragment, "res/shaders/mesh_fs.glsl");
	program = new Program(m_vert, m_frag);
	program->SetAttribute("position", 0);
	program->SetAttribute("normal", 1);
	program->SetAttribute("tex_coords", 2);

	this->vao->BindAttribute(program->GetAttribute("position"), *vbo, GL_FLOAT, 3, sizeof(MeshVertex), 0);
	this->vao->BindAttribute(program->GetAttribute("normal"), *vbo, GL_FLOAT, 3, sizeof(MeshVertex), offsetof(MeshVertex, Normal));
	this->vao->BindAttribute(program->GetAttribute("tex_coords"), *vbo, GL_FLOAT, 2, sizeof(MeshVertex), offsetof(MeshVertex, TexCoords));
}

void Mesh::Init()
{
	this->model_matrix = glm::mat4(1.0f);
	this->model_matrix = glm::translate(this->model_matrix, glm::vec3(0.0f, -300.0f, -500.0f));
	this->model_matrix = glm::scale(this->model_matrix, glm::vec3(1.0f));
	this->model_matrix = glm::rotate(this->model_matrix, glm::radians(70.0f), glm::vec3(1.0f));

	glUseProgram(*(this->program));
	glBindVertexArray(*vao);
	program->SetUniform(program->GetUniform("modelMatrix"), this->model_matrix);
	glUseProgram(0);
}

void Mesh::Render(Camera camera)
{
	this->view_matrix = camera.GetViewMatrix();
	this->projection_matrix = glm::perspective(glm::radians(camera.GetFieldOfView()),
		(float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, camera.GetNearPlane(), camera.GetFarPlane());

	int normal_map = 1;
	int specular_map = 1;
	int ambient_map = 1;

	for (int i = 0; i < this->textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		std::string number;
		std::string name = textures[i].Type;
		if (name == "texture_normal") {
			number = std::to_string(normal_map++);
		}
		else if (name == "texture_specular") {
			number = std::to_string(specular_map++);
		}
		else if (name == "texture_ambient") {
			number = std::to_string(ambient_map++);
		}

		glBindTexture(GL_TEXTURE_2D, textures[i].ID);
		glUniform1i(glGetUniformLocation(*this->program, ("material." + name + number).c_str()), i);
	}

	glUseProgram(*this->program);
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glBindVertexArray(*vao);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
	glUseProgram(0);

	for (int i = 0; i < this->textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void Mesh::Update(glm::mat4 view_matrix, glm::mat4 projection_matrix)
{
	this->view_matrix = view_matrix;
	this->projection_matrix = projection_matrix;
	glUseProgram(*(this->program));
	program->SetUniform(program->GetUniform("viewMatrix"), this->view_matrix);
	program->SetUniform(program->GetUniform("projectionMatrix"), this->projection_matrix);
	glUseProgram(0);
}

void Mesh::Update(float angle, glm::vec3 pivot, glm::vec3 axis) { }

void Mesh::Clean()
{
	this->vertices.clear();
	this->indices.clear();
	this->textures.clear();
	this->vao = nullptr;
	this->vbo = nullptr;
	this->ebo = nullptr;
	this->program = nullptr;
	this->model_matrix = glm::mat4(1.0f);
	this->view_matrix = glm::mat4(1.0f);
	this->projection_matrix = glm::mat4(1.0f);
}

Mesh::~Mesh() { this->Clean(); }
