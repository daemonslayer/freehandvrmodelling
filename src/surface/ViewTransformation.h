#pragma once
#include <iostream>
#include <fstream>
#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtx/vector_angle.hpp>
#include "AverageFrame.h"

#define FRAME_WINDOW 20
	
class ViewTransformation
{
private:
	AverageFrame current_frame;
	AverageFrame previous_frame;
	AverageFrame buffer_frame;

public:
	ViewTransformation();
	virtual ~ViewTransformation();
	
	void AddFrame();
	void SetAxis(const LEAP_TRACKING_EVENT* prev_avgframe, const LEAP_TRACKING_EVENT* curr_avgframe);
	glm::vec3 GetAxis();
	void SetPivot(const LEAP_TRACKING_EVENT* curr_avgframe);
	glm::vec3 GetPivot();
	void SetDistanceBetweenHands(const LEAP_TRACKING_EVENT* curr_avgframe);
	float GetDistanceBetweenHands();
	void SetAngle(const LEAP_TRACKING_EVENT* prev_avgframe, const LEAP_TRACKING_EVENT* curr_avgframe);
	float GetAngle();
	glm::vec3 GetZoom();
	void Clear();

};

