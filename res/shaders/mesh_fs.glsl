#version 450

in vec2 ftex_coords;
in vec3 fposition;
in vec3 fnormal;

struct Material
{
	sampler2D texture_normal1;
	sampler2D texture_specular1;
	sampler2D texture_color1;
	float shininess;
	float transparency;
};

struct PointLight 
{
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular; 

    float constant;
    float linear;
    float quadratic;      
};

/*vec3 CalcPointLight(PointLight light, vec3 normal, vec3 view_dir, vec3 frag_pos)
{
	vec3 light_dir = normalize(light.position - frag_pos);
	float diff = max(dot(normal, light_dir), 0.0);
	vec3 reflect_dir = reflect(-light_dir, normal);
	float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
	float distance = length(light.position - frag_pos);
	float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	vec3 ambient = light.ambient * vec3(texture(material.texture_diffuse1, text_coords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, text_coords));
    vec3 specular = light.specular * spec * vec3(texture(material.texture_specular1, text_coords));

	ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    	
	return (ambient + diffuse + specular);
}*/

uniform Material material;

void main()
{
	vec3 normal = vec3(texture(material.texture_normal1, ftex_coords));
	vec3 specular = vec3(texture(material.texture_specular1, ftex_coords));
	vec3 color = vec3(texture(material.texture_color1, ftex_coords));

	// vec4 calc_color = vec4( CalcPointLight(point_light, normal, view_dir, frag_pos), 1.0);

	gl_FragColor = vec4(normal + color + specular, 1.0f);
}