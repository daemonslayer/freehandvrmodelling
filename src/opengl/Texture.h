#pragma once
#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

class Texture
{
private:
	GLuint texture_programme;
	int image_width;
	int image_height;
	int image_channels;

public:
	Texture();
	Texture(const Texture& tex);
	Texture(std::string image_url, int internalFormat = GL_RGBA);

	~Texture();

	operator GLuint() const;
	const Texture& operator=(const Texture& other);

	void Image2D(const GLvoid* data, int type, int format, unsigned int width, 
		unsigned int height, int internalFormat);

	void SetWrapping(int s);
	void SetWrapping(int s, int t);
	void SetWrapping(int s, int t, int r);

	void SetFilters(int min, int mag);
	void SetBorderColor(const glm::vec4& color);

	void GenerateMipmaps();
};
