/***************************************************************************
**                                                                        **
**  FreehandModelling - A Virtual Reality Application                     **
**  Copyright (C) 2018 Graphics Research Group, IIIT Delhi                **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Kapil Gupta                                          **
**           E-mail: kapilg@iiitd.ac.in                                   **
**           Date  : 17.11.2018                                           **
****************************************************************************/
#include "IGestureDetectorLogicGate.h"
#include <assert.h>
#include <iostream>


IGestureDetectorLogicGate::IGestureDetectorLogicGate() {}

bool IGestureDetectorLogicGate::CheckState(const LEAP_TRACKING_EVENT* frame) {
	if (last_update_gesture == 0) {
		if (this->gesture_detectors[0]->IsGestureActive(frame)) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		bool state = true;
		std::cout << (int)this->gesture_detectors[this->last_update_gesture]->Type() << std::endl;

		if (this->detector_logic_gates[this->last_update_gesture] == LOGICGATE_TYPE::AND_GATE) {
			return state && (this->gesture_detectors[this->last_update_gesture]->IsGestureActive(frame)
				&& !this->detector_negate[this->last_update_gesture]);
		}
		else {
			return state || (this->gesture_detectors[this->last_update_gesture]->IsGestureActive(frame)
				&& !this->detector_negate[this->last_update_gesture]);
		}
	}

}

void IGestureDetectorLogicGate::CheckLeapFrame(const LEAP_TRACKING_EVENT* frame)
{
	if (this->last_update_gesture == this->gesture_detectors.size()) {
		this->current_state = true;
		this->is_gesture_active = true;
		this->is_gesture_complete = true;
		this->is_gesture_started = false;
	}

	else if (this->last_update_gesture == 0) {
		if (this->CheckState(frame)) {
			this->last_update_gesture += 1;
			this->time_of_last_gesture_detection = std::chrono::high_resolution_clock::now();
			this->current_state = true;
			this->is_gesture_active = false;
			this->is_gesture_complete = false;
			this->is_gesture_started = true;
		}
	}

	else {
		auto now = std::chrono::high_resolution_clock::now();
		auto window_spent = now - this->time_of_last_gesture_detection;
		if (window_spent > this->valid_window_for_gesture[this->last_update_gesture]) {
			this->ResetCompositeGesture();
		}
		else {
			for (int i = this->last_update_gesture; i < this->gesture_detectors.size(); ++i) {
				if (this->CheckState(frame)) {
					this->last_update_gesture += 1;
					this->time_of_last_gesture_detection = std::chrono::high_resolution_clock::now();
					this->current_state = true;
					this->is_gesture_active = false;
					this->is_gesture_complete = false;
				}
				else {
					break;
				}
			}
		}
	}
}

void IGestureDetectorLogicGate::ActiveHand(HAND_TYPE hand_type) { this->hand_type = hand_type; }

void IGestureDetectorLogicGate::InitCompositeGesture()
{
	this->hand_type = HAND_TYPE::BOTH_HANDS;
	this->is_gesture_active = false;
	this->last_update_frame = -1;
	this->last_update_gesture = 0;
	this->did_change = false;
	this->is_gesture_started = false;
	this->is_gesture_complete = false;
	this->current_state = false;
}

void IGestureDetectorLogicGate::ResetCompositeGesture()
{
	this->hand_type = HAND_TYPE::BOTH_HANDS;
	this->is_gesture_active = false;
	this->last_update_frame = -1;
	this->last_update_gesture = 0;
	this->did_change = false;
	this->is_gesture_started = false;
	this->is_gesture_complete = false;
	this->current_state = false;
}

void IGestureDetectorLogicGate::AddGesture(GESTURE_TYPE gesture_type, LOGICGATE_TYPE gesture_logicgate,
	bool detector_negate, std::chrono::seconds gesture_window, HAND_TYPE hand_type)
{
	switch (gesture_type) {
		case GESTURE_TYPE::HOLD_GESTURE:
		{
			HoldGestureDetector* holdgestureDetector = new HoldGestureDetector();
			holdgestureDetector->ActiveHand(hand_type);
			this->gesture_detectors.push_back(holdgestureDetector);
			break;
		}

		case GESTURE_TYPE::EXTENDEDFINGER_GESTURE:
		{
			ExtendedFingerDetector* extendedfingerDetector = new ExtendedFingerDetector();
			extendedfingerDetector->ActiveHand(hand_type);
			this->gesture_detectors.push_back(extendedfingerDetector);
			break;
		}

		case GESTURE_TYPE::PINCH_GESTURE:
		{
			PinchDetector* pinchDetector = new PinchDetector();
			pinchDetector->ActiveHand(hand_type);
			this->gesture_detectors.push_back(pinchDetector);
			break;
		}

		case GESTURE_TYPE::SWIPE_GESTURE:
		{
			SwipeDetector* swipeDetector = new SwipeDetector();
			swipeDetector->ActiveHand(hand_type);
			this->gesture_detectors.push_back(swipeDetector);
			break;
		}

		case GESTURE_TYPE::FINGERPOINT_GESTURE:
		{
			FingerPointDetector* fingerpointDetector = new FingerPointDetector();
			fingerpointDetector->ActiveHand(hand_type);
			this->gesture_detectors.push_back(fingerpointDetector);
			break;
		}
	}

	this->detector_logic_gates.push_back(gesture_logicgate);
	this->detector_negate.push_back(detector_negate);
	this->valid_window_for_gesture.push_back(gesture_window);
}

bool IGestureDetectorLogicGate::IsGestureActive(const LEAP_TRACKING_EVENT* frame)
{
	if (frame->nHands <= 0) return false;
	this->CheckLeapFrame(frame);
	if (this->is_gesture_active && this->is_gesture_complete) {
		this->ResetCompositeGesture();
		return true;
	}
	else {
		return false;
	}
}

