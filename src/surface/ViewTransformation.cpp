#include "ViewTransformation.h"



ViewTransformation::ViewTransformation() { this->Clear(); }

void ViewTransformation::AddFrame()
{
	if (this->current_frame.IsEmpty() && 
		this->buffer_frame.IsEmpty() && this->previous_frame.IsEmpty()) {
		current_frame.AddCurrentFrame();
	}
	else if (!this->current_frame.IsEmpty() &&
			this->buffer_frame.IsEmpty() && this->previous_frame.IsEmpty()) {
		this->buffer_frame.AddFrame(this->current_frame.GetFirstFrame());
		this->current_frame.RemoveFirstFrame();
		this->current_frame.AddCurrentFrame();
	}
	else if ( (!this->current_frame.IsEmpty() &&
			!this->buffer_frame.IsEmpty() && this->previous_frame.IsEmpty()) || 
			(!this->current_frame.IsFull() && !this->buffer_frame.IsFull() && 
			!this->previous_frame.IsFull()) ) {
		this->previous_frame.AddFrame(this->buffer_frame.GetFirstFrame());
		this->buffer_frame.RemoveFirstFrame();
		this->buffer_frame.AddFrame(this->current_frame.GetFirstFrame());
		this->current_frame.RemoveFirstFrame();
		this->current_frame.AddCurrentFrame();
	}
	else if (this->current_frame.IsFull() &&
			this->buffer_frame.IsFull() && this->previous_frame.IsFull()) {
		this->previous_frame.RemoveFirstFrame();
		this->previous_frame.AddFrame(this->buffer_frame.GetFirstFrame());
		this->buffer_frame.RemoveFirstFrame();
		this->buffer_frame.AddFrame(this->current_frame.GetFirstFrame());
		this->current_frame.RemoveFirstFrame();
		this->current_frame.AddCurrentFrame();
	}
	
	assert(("[-] CurrentFrame larger than required, assertion failed", current_frame.Size() <= POOL_SIZE));
	assert(("[-] PreviousFrame larger than required, assertion failed", previous_frame.Size() <= POOL_SIZE));
	assert(("[-] Buffer Window larger than required, assertion failed", buffer_frame.Size() <= FRAME_WINDOW));
	
	const LEAP_TRACKING_EVENT* prev_avgframe = this->previous_frame.GetAverageFrame();
	const LEAP_TRACKING_EVENT* curr_avgframe = this->current_frame.GetAverageFrame();

	this->SetAxis(prev_avgframe, curr_avgframe);
	this->SetDistanceBetweenHands(curr_avgframe);
	this->SetPivot(curr_avgframe);
	this->SetAngle(prev_avgframe, curr_avgframe);
}

void ViewTransformation::SetAxis(const LEAP_TRACKING_EVENT* prev_avgframe, const LEAP_TRACKING_EVENT* curr_avgframe)
{
	if (prev_avgframe->nHands > 0 && curr_avgframe->nHands > 0) {
		glm::vec3 a = glm::normalize(glm::vec3(
			prev_avgframe->pHands[1].palm.position.x - prev_avgframe->pHands[0].palm.position.x,
			prev_avgframe->pHands[1].palm.position.y - prev_avgframe->pHands[0].palm.position.y,
			prev_avgframe->pHands[1].palm.position.z - prev_avgframe->pHands[0].palm.position.z));

		glm::vec3 b = glm::normalize(glm::vec3(
			curr_avgframe->pHands[1].palm.position.x - curr_avgframe->pHands[0].palm.position.x,
			curr_avgframe->pHands[1].palm.position.y - curr_avgframe->pHands[0].palm.position.y,
			curr_avgframe->pHands[1].palm.position.z - curr_avgframe->pHands[0].palm.position.z));

		this->previous_frame.axis = this->current_frame.axis;
		this->current_frame.axis = glm::cross(a, b);
	}
}

void ViewTransformation::SetPivot(const LEAP_TRACKING_EVENT* curr_avgframe)
{
	if (curr_avgframe->nHands > 0) {
		this->previous_frame.pivot = this->current_frame.pivot;
		this->current_frame.pivot = glm::vec3(
			curr_avgframe->pHands[1].palm.position.x + curr_avgframe->pHands[0].palm.position.x,
			curr_avgframe->pHands[1].palm.position.y + curr_avgframe->pHands[0].palm.position.y,
			curr_avgframe->pHands[1].palm.position.z + curr_avgframe->pHands[0].palm.position.z) / glm::vec3(2.0f, 2.0f, 2.0f);
	}
}

void ViewTransformation::SetDistanceBetweenHands(const LEAP_TRACKING_EVENT* curr_avgframe)
{
	this->previous_frame.hands_distance = this->current_frame.hands_distance;
	this->current_frame.hands_distance = glm::vec3(0.0f);
	glm::vec3 distance = glm::vec3(
		curr_avgframe->pHands[1].palm.position.x - curr_avgframe->pHands[0].palm.position.x,
		curr_avgframe->pHands[1].palm.position.y - curr_avgframe->pHands[0].palm.position.y,
		curr_avgframe->pHands[1].palm.position.z - curr_avgframe->pHands[0].palm.position.z);

	if (std::abs(glm::length(distance) - glm::length(this->current_frame.hands_distance)) > 1.0f) this->current_frame.hands_distance = distance;
}

void ViewTransformation::SetAngle(const LEAP_TRACKING_EVENT* prev_avgframe, const LEAP_TRACKING_EVENT* curr_avgframe)
{
	// log_frame(prev_avgframe, "previous frame");
	// log_frame(curr_avgframe, "current frame");
	if (prev_avgframe->nHands > 0 && curr_avgframe->nHands > 0) {

		glm::vec3 vector_a = glm::normalize(glm::vec3(
			prev_avgframe->pHands[1].palm.position.x - prev_avgframe->pHands[0].palm.position.x,
			prev_avgframe->pHands[1].palm.position.y - prev_avgframe->pHands[0].palm.position.y,
			prev_avgframe->pHands[1].palm.position.z - prev_avgframe->pHands[0].palm.position.z));

		glm::vec3 vector_b = glm::normalize(glm::vec3(
			curr_avgframe->pHands[1].palm.position.x - curr_avgframe->pHands[0].palm.position.x,
			curr_avgframe->pHands[1].palm.position.y - curr_avgframe->pHands[0].palm.position.y,
			curr_avgframe->pHands[1].palm.position.z - curr_avgframe->pHands[0].palm.position.z));

		this->previous_frame.angle = this->current_frame.angle;
		this->current_frame.angle = glm::angle(vector_a, vector_b);
		float axis_vector_angle = glm::angle(glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f)), glm::normalize(glm::cross(vector_a, vector_b)));

		if (axis_vector_angle > glm::radians(90.0f)) {
			this->current_frame.angle *= -1;
			// std::cout << "angle is higher than 90.0" << std::endl;
		}

	}

}

glm::vec3 ViewTransformation::GetAxis() { return this->current_frame.axis; }
glm::vec3 ViewTransformation::GetPivot() { return this->current_frame.pivot; }
float ViewTransformation::GetDistanceBetweenHands() { return glm::length(this->current_frame.hands_distance); }
float ViewTransformation::GetAngle() { return this->current_frame.angle; }
glm::vec3 ViewTransformation::GetZoom()
{
	glm::vec3 delta_distance = this->current_frame.hands_distance - this->previous_frame.hands_distance;
	if (std::abs(glm::length(delta_distance)) > 1.0f) return delta_distance;
	else return glm::vec3(1.0f);
}

void ViewTransformation::Clear()
{
	this->current_frame.Clear();
	this->previous_frame.Clear();
}

ViewTransformation::~ViewTransformation() { this->Clear(); }