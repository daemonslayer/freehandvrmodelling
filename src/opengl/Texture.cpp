#include "Texture.h"


Texture::Texture() { glGenTextures(1, &this->texture_programme); }
Texture::Texture(const Texture& tex) {}
Texture::Texture(std::string image_url, int internalFormat) : 
	image_width(0),
	image_height(0),
	image_channels(0)
{
	GLint restoreId; 

	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load(image_url.c_str(), &image_width, &image_height, &image_channels, 0);

	glGetIntegerv(GL_TEXTURE_BINDING_2D, &restoreId);
	glGenTextures(1, &this->texture_programme);
	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, this->image_width, this->image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, restoreId);
}

Texture::operator GLuint() const { return this->texture_programme; }
const Texture& Texture::operator=(const Texture& other) { return *this; }

void Texture::Image2D(const GLvoid* data, int type, int format, 
	unsigned int width, unsigned int height, int internalFormat)
{
	GLint restoreId; 
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &restoreId);

	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, data);

	glBindTexture(GL_TEXTURE_2D, restoreId);
}

void Texture::SetWrapping(int s)
{
	GLint restoreId;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &restoreId);

	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s);

	glBindTexture(GL_TEXTURE_2D, restoreId);
}

void Texture::SetWrapping(int s, int t)
{
	GLint restoreId; glGetIntegerv( GL_TEXTURE_BINDING_2D, &restoreId );

	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t);

	glBindTexture( GL_TEXTURE_2D, restoreId );
}

void Texture::SetWrapping(int s, int t, int r)
{
	GLint restoreId; glGetIntegerv( GL_TEXTURE_BINDING_2D, &restoreId );

	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, r);

	glBindTexture( GL_TEXTURE_2D, restoreId );
}

void Texture::SetFilters(int min, int mag)
{
	GLint restoreId; 
	glGetIntegerv( GL_TEXTURE_BINDING_2D, &restoreId );

	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag);

	glBindTexture( GL_TEXTURE_2D, restoreId );
}

void Texture::SetBorderColor(const glm::vec4& color)
{
	GLint restoreId; glGetIntegerv( GL_TEXTURE_BINDING_2D, &restoreId );

	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	float col[4] = { color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f, color[3] / 255.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, col);

	glBindTexture( GL_TEXTURE_2D, restoreId );
}

void Texture::GenerateMipmaps()
{
	GLint restoreId; glGetIntegerv( GL_TEXTURE_BINDING_2D, &restoreId );

	glBindTexture(GL_TEXTURE_2D, this->texture_programme);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture( GL_TEXTURE_2D, restoreId );
}

Texture::~Texture() {}